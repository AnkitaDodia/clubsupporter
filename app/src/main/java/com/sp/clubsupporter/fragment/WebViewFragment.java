package com.sp.clubsupporter.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;

import com.sp.clubsupporter.R;
import com.sp.clubsupporter.activity.DashBoardActivity;
import com.sp.clubsupporter.activity.LoginActivity;
import com.sp.clubsupporter.common.BaseActivity;

import im.delight.android.webview.AdvancedWebView;

public class WebViewFragment extends Fragment implements AdvancedWebView.Listener{

    private final String TAG = "WebViewFragment";
    AdvancedWebView webview_master;
    DashBoardActivity mContext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_web_view, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        mContext = (DashBoardActivity) getActivity();

        webview_master = view.findViewById(R.id.webview_master);

        webview_master.getSettings();
        webview_master.setBackgroundColor(Color.TRANSPARENT);
        webview_master.addPermittedHostname("cssupporter.azurewebsites.net");
        webview_master.addJavascriptInterface(new WebAppInterface(mContext), "Android");

        Log.e("WEBVIEW_URL","mCurrentUrl : "+BaseActivity.mCurrentUrl);

        webview_master.setListener(getActivity(), this);
        webview_master.loadUrl(BaseActivity.mCurrentUrl);


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        webview_master.onActivityResult(requestCode, resultCode, intent);
        // ...
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {

        Log.e(TAG,"onPageFinished");
        mContext.showWaitIndicator(true);
//        mContext.showProgressDialog();
    }

    @Override
    public void onPageFinished(String url) {

        mContext.showWaitIndicator(false);
//        mContext.dismissProgressDialog();
        Log.e(TAG,"onPageFinished");

      /*  if (BaseActivity.mIsLogOut){

            BaseActivity.mIsLogOut = false;

            mContext.setLogin(0);

            Intent i = new Intent(mContext, LoginActivity.class);
            startActivity(i);
            mContext.finish();
        }*/
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {
//        mContext.dismissProgressDialog();
        mContext.showWaitIndicator(false);
        Log.e(TAG,"onPageError");
    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) { }

    @Override
    public void onExternalPageRequest(String url) {

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    class WebAppInterface {
        Context mCtx;

        /** Instantiate the interface and set the context */
        WebAppInterface(Context c) {
            mCtx = c;
        }

        /** Show a toast from the web page */
        @JavascriptInterface
        public void showToast(final String WebMessage) {

            mContext.runOnUiThread(new Runnable() {

                @Override
                public void run() {

//                    Toast.makeText(mCtx, WebMessage, Toast.LENGTH_SHORT).show();
                    Log.e("WebAppInterface", "value :  "+WebMessage);

//            value :  Balance:27
//            value :  ClubId:85
//            value :  Logout:true

//                    {'ClubsUpdated': true}

                    String[] separated = WebMessage.trim().split(":");
                    String Message  = separated[0];
                    String Value  = separated[1];

                    if(Message.equalsIgnoreCase("Balance")){

                        BaseActivity.mBalance = Double.parseDouble(Value);
//                        BaseActivity.mBalance = Integer.parseInt(Value);
                        mContext.UpdtaeBalanceLable();

                    }else if(Message.equalsIgnoreCase("ClubId")){

                        BaseActivity.mSwitchClub = true;

                        BaseActivity.mClubID = Integer.parseInt(Value);

                        mContext.setClubId(BaseActivity.mClubID);

                        Intent i = new Intent(mContext, DashBoardActivity.class);
                        startActivity(i);

                    }else if(Message.equalsIgnoreCase("Logout")){

                        if(Value.equalsIgnoreCase("true")){

//                    BaseActivity.mIsLogOut = false;

                            mContext.setLogin(0);

                            Intent i = new Intent(mContext, LoginActivity.class);
                            startActivity(i);
                            mContext.finish();

                        }

                    }else if(Message.equalsIgnoreCase("ClubsUpdated")){
//                        ClubsUpdated:true
//                        BaseActivity.mSwitchClub = true;
//
//                        BaseActivity.mClubID = Integer.parseInt(Value);
//
//                        mContext.setClubId(BaseActivity.mClubID);

                        Intent i = new Intent(mContext, DashBoardActivity.class);
                        startActivity(i);

                    }

                }
            });



            
        }
    }

}

