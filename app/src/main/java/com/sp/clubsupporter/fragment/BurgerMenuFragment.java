package com.sp.clubsupporter.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sp.clubsupporter.R;
import com.sp.clubsupporter.activity.DashBoardActivity;
import com.sp.clubsupporter.activity.LoginActivity;
import com.sp.clubsupporter.adapter.MyClubDisplayAdapter;
import com.sp.clubsupporter.common.BaseActivity;
import com.sp.clubsupporter.models.ClubCodeError;
import com.sp.clubsupporter.models.UpdateBalance;
import com.sp.clubsupporter.restinterface.RestInterface;
import com.sp.clubsupporter.utility.TypeFaces;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class BurgerMenuFragment extends Fragment implements View.OnClickListener {

    DashBoardActivity mContext;
    LinearLayout ll_parent_burgermenu, ll_games, ll_my_games, ll_my_club, ll_add_new_club, ll_my_settings, ll_update_balance, ll_debitcard, ll_responsible_gambling, ll_logout,
            ll_my_details, ll_my_transactions, ll_my_password, ll_cashflow, ll_withdraw, ll_take_a_break;

    TextView icon_games, icon_my_games, icon_my_club, icon_add_new_club, icon_quick_deposit, icon_my_settings, icon_my_details,
            icon_my_transaction, icon_my_password, icon_my_cashflow, icon_withdraw, icon_take_a_break, icon_responsible_gambling, icon_logout;
    TextView txt_last_login, txt_games, txt_my_games, txt_my_club, txt_add_new_club, txt_my_settings, txt_update_balance, txt_debitcard, txt_responsible_gambling, txt_logout,
            txt_my_details, txt_my_transactions, txt_my_password, txt_cashflow, txt_withdraw, txt_take_a_break;
    EditText edt_nav_amount, edt_nav_cvs;
    Button btn_deposit;

    RecyclerView rv_my_club;
    private MyClubDisplayAdapter mMyClubDisplayAdapter;

    //    https://cssupporterapi.azurewebsites.net/mydashboard?id={id}&email={email}
//    https://cssupporter.azurewebsites.net/mydashboard?id=c34f642b-3b05-42eb-9acb-c6ac74f88438&email=csuser005@gmail.com

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_burger_menu, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (DashBoardActivity) getActivity();

        initViews(view);
        setViewListner();

//        setIconTint();
        setLinksColor();
    }

    private void setLinksColor() {

        txt_last_login.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));

        txt_games.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));
        txt_my_games.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));
        txt_my_club.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));
        txt_my_settings.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));
        txt_update_balance.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));
        txt_debitcard.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));
        txt_responsible_gambling.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));
        txt_logout.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));
        txt_my_details.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));
        txt_my_transactions.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));
        txt_my_password.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));
        txt_cashflow.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));
        txt_withdraw.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));
        txt_take_a_break.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));

        txt_add_new_club.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));

        icon_games.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));
        icon_my_club.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));
        icon_my_games.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));
        icon_add_new_club.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));
        icon_quick_deposit.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));
        icon_my_settings.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));

        icon_my_details.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));
        icon_my_transaction.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));
        icon_my_password.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));
        icon_my_cashflow.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));
        icon_withdraw.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));
        icon_take_a_break.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));

        icon_responsible_gambling.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));
        icon_logout.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));

        ll_parent_burgermenu.setBackgroundColor(Color.parseColor(BaseActivity.NAVIGATION_COLOR));
    }



    private void initViews(View view) {

        ll_parent_burgermenu = view.findViewById(R.id.ll_parent_burgermenu);

        ll_games = view.findViewById(R.id.ll_games);
        ll_my_games = view.findViewById(R.id.ll_my_games);
        ll_my_club = view.findViewById(R.id.ll_my_club);
        ll_add_new_club = view.findViewById(R.id.ll_add_new_club);
        ll_my_settings = view.findViewById(R.id.ll_my_settings);
        ll_update_balance = view.findViewById(R.id.ll_update_balance);
        ll_debitcard = view.findViewById(R.id.ll_debitcard);
        ll_responsible_gambling = view.findViewById(R.id.ll_responsible_gambling);
        ll_logout = view.findViewById(R.id.ll_logout);

        ll_my_details = view.findViewById(R.id.ll_my_details);
        ll_my_transactions = view.findViewById(R.id.ll_my_transactions);
        ll_my_password = view.findViewById(R.id.ll_my_password);
        ll_cashflow = view.findViewById(R.id.ll_cashflow);
        ll_withdraw = view.findViewById(R.id.ll_withdraw);
        ll_take_a_break = view.findViewById(R.id.ll_take_a_break);



        txt_last_login = view.findViewById(R.id.txt_last_login);

        txt_games = view.findViewById(R.id.txt_games);
        txt_my_games = view.findViewById(R.id.txt_my_games);
        txt_my_club = view.findViewById(R.id.txt_my_club);
        txt_add_new_club = view.findViewById(R.id.txt_add_new_club);
        txt_my_settings = view.findViewById(R.id.txt_my_settings);
        txt_update_balance = view.findViewById(R.id.txt_update_balance);
        txt_debitcard = view.findViewById(R.id.txt_debitcard);
        txt_responsible_gambling = view.findViewById(R.id.txt_responsible_gambling);
        txt_logout = view.findViewById(R.id.txt_logout);

        txt_my_details = view.findViewById(R.id.txt_my_details);
        txt_my_transactions = view.findViewById(R.id.txt_my_transactions);
        txt_my_password = view.findViewById(R.id.txt_my_password);
        txt_cashflow = view.findViewById(R.id.txt_cashflow);
        txt_withdraw = view.findViewById(R.id.txt_withdraw);
        txt_take_a_break = view.findViewById(R.id.txt_take_a_break);



        icon_games = view.findViewById(R.id.icon_games);
        icon_my_club = view.findViewById(R.id.icon_my_club);
        icon_my_games = view.findViewById(R.id.icon_my_games);
        icon_add_new_club = view.findViewById(R.id.icon_add_new_club);
        icon_quick_deposit = view.findViewById(R.id.icon_quick_deposit);
        icon_my_settings = view.findViewById(R.id.icon_my_settings);

        icon_my_details = view.findViewById(R.id.icon_my_details);
        icon_my_transaction = view.findViewById(R.id.icon_my_transaction);
        icon_my_password = view.findViewById(R.id.icon_my_password);
        icon_my_cashflow = view.findViewById(R.id.icon_my_cashflow);
        icon_withdraw = view.findViewById(R.id.icon_withdraw);
        icon_take_a_break = view.findViewById(R.id.icon_take_a_break);

        icon_responsible_gambling = view.findViewById(R.id.icon_responsible_gambling);
        icon_logout = view.findViewById(R.id.icon_logout);


        edt_nav_amount = view.findViewById(R.id.edt_nav_amount);
        edt_nav_cvs = view.findViewById(R.id.edt_nav_cvs);
        btn_deposit = view.findViewById(R.id.btn_deposit);

        rv_my_club = view.findViewById(R.id.rv_my_club);
        rv_my_club.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        mMyClubDisplayAdapter = new MyClubDisplayAdapter(mContext, BaseActivity.mMyClubDisplayList);
        rv_my_club.setAdapter(mMyClubDisplayAdapter);

//        edt_nav_amount.setFilters(new InputFilter[]{ new InputFilterMinMax("4", "101")});


        NumberFormat mNumberFormat = NumberFormat.getInstance();
        mNumberFormat.setMinimumFractionDigits(2);
        mNumberFormat.setMaximumFractionDigits(2);
        String Balance = mNumberFormat.format(BaseActivity.mBalance);
//        txt_update_balance.setText("Quick Deposit: " + BaseActivity.mCurrencySymbol + Balance);


        Log.e("Last Login","date :  "+BaseActivity.mLastLoginDate);
        if(BaseActivity.mLastLoginDate != null){

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            try {
                Date date = format.parse(BaseActivity.mLastLoginDate);
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yy HH:mm:ss");
//            String formatedDate = dateFormat.format(date);
                txt_last_login.setText("Last Login: "+dateFormat.format(date));//+" Current Balance: " + BaseActivity.mCurrencySymbol + Balance
                System.out.println(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


        edt_nav_amount.setHint("0.00 (min "+BaseActivity.mCurrencySymbol+"5, max "+BaseActivity.mCurrencySymbol+"100)");

        icon_games.setTypeface(TypeFaces.getTypeFace(mContext, "fontawesome-webfont.ttf"));
        icon_my_games.setTypeface(TypeFaces.getTypeFace(mContext, "fontawesome-webfont.ttf"));
        icon_my_club.setTypeface(TypeFaces.getTypeFace(mContext, "fontawesome-webfont.ttf"));
        icon_add_new_club.setTypeface(TypeFaces.getTypeFace(mContext, "fontawesome-webfont.ttf"));
        icon_quick_deposit.setTypeface(TypeFaces.getTypeFace(mContext, "fontawesome-webfont.ttf"));
        icon_my_settings.setTypeface(TypeFaces.getTypeFace(mContext, "fontawesome-webfont.ttf"));

        icon_my_details.setTypeface(TypeFaces.getTypeFace(mContext, "fontawesome-webfont.ttf"));
        icon_my_transaction.setTypeface(TypeFaces.getTypeFace(mContext, "fontawesome-webfont.ttf"));
        icon_my_password.setTypeface(TypeFaces.getTypeFace(mContext, "fontawesome-webfont.ttf"));
        icon_my_cashflow.setTypeface(TypeFaces.getTypeFace(mContext, "fontawesome-webfont.ttf"));
        icon_withdraw.setTypeface(TypeFaces.getTypeFace(mContext, "fontawesome-webfont.ttf"));
        icon_take_a_break.setTypeface(TypeFaces.getTypeFace(mContext, "fontawesome-webfont.ttf"));

        icon_responsible_gambling.setTypeface(TypeFaces.getTypeFace(mContext, "fontawesome-webfont.ttf"));
        icon_logout.setTypeface(TypeFaces.getTypeFace(mContext, "fontawesome-webfont.ttf"));


        setCustomDrawable(Color.parseColor(BaseActivity.LINK_COLOR), Color.BLUE);
        btn_deposit.setTextColor(Color.parseColor(BaseActivity.NAVIGATION_COLOR));
    }

    private void setViewListner() {

        ll_games.setOnClickListener(this);
        ll_my_games.setOnClickListener(this);
        ll_add_new_club.setOnClickListener(this);
        ll_my_settings.setOnClickListener(this);
        ll_responsible_gambling.setOnClickListener(this);
        ll_logout.setOnClickListener(this);


        btn_deposit.setOnClickListener(this);

        ll_my_details.setOnClickListener(this);
        ll_my_transactions.setOnClickListener(this);
        ll_my_password.setOnClickListener(this);
        ll_cashflow.setOnClickListener(this);
        ll_withdraw.setOnClickListener(this);
        ll_take_a_break.setOnClickListener(this);

        rv_my_club.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rv_my_club, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                if(position == 0){

                    BaseActivity.mCurrentUrl = BaseActivity.MY_CLUBS_URL;
                    mContext.ChangeMenuIcon();
                    mContext.replaceFragmentOpen(new WebViewFragment());

                }else {

                    BaseActivity.mSwitchClub = true;

                    Log.e("getClubId", "getClubId : " + BaseActivity.mMyClubDisplayList.get(position).getClubId());
//                BaseActivity.mMyClubDisplayList.get(position).getClubId();
                    BaseActivity.mClubID = BaseActivity.mMyClubDisplayList.get(position).getClubId();
//                mContext.sendWhiteLablingRequest();

                    mContext.setClubId(BaseActivity.mClubID);

                    Intent i = new Intent(mContext, DashBoardActivity.class);
                    startActivity(i);

                }

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onClick(View v) {

        //    https://cssupporterapi.azurewebsites.net/mydashboard?id={id}&email={email}
//    https://cssupporter.azurewebsites.net/mydashboard?id=c34f642b-3b05-42eb-9acb-c6ac74f88438&email=csuser005@gmail.com

        String URL_EXTENTIONS = "";
//                "id="+BaseActivity.mUserID+"&email=" + BaseActivity.mEmailID;
//        String URL_EXTENTIONS = "id="+BaseActivity.mAccessID+"&email="+BaseActivity.mEmailID;

        switch (v.getId()) {
            case R.id.ll_games:

                BaseActivity.mCurrentUrl = BaseActivity.GAMES_DASHBOARD_URL + URL_EXTENTIONS;

                mContext.ChangeMenuIcon();
                mContext.replaceFragmentOpen(new WebViewFragment());

                break;

            case R.id.ll_my_games:

                BaseActivity.mCurrentUrl = BaseActivity.MY_GAMES_URL + URL_EXTENTIONS;
                mContext.ChangeMenuIcon();
                mContext.replaceFragmentOpen(new WebViewFragment());

                break;

            case R.id.ll_add_new_club:

                BaseActivity.mCurrentUrl = BaseActivity.ADD_NEW_CLUBS_URL + URL_EXTENTIONS;
                mContext.ChangeMenuIcon();
                mContext.replaceFragmentOpen(new WebViewFragment());

                break;

            case R.id.ll_my_settings:

                BaseActivity.mCurrentUrl = BaseActivity.MY_SETTINGS_URL + URL_EXTENTIONS;
                mContext.ChangeMenuIcon();
                mContext.replaceFragmentOpen(new WebViewFragment());

                break;

            case R.id.btn_deposit:

                DepositeBalance();

                break;

            case R.id.ll_my_details:

                BaseActivity.mCurrentUrl = BaseActivity.MY_DETAILS_URL + URL_EXTENTIONS;
                mContext.ChangeMenuIcon();
                mContext.replaceFragmentOpen(new WebViewFragment());

                break;

            case R.id.ll_my_transactions:

                BaseActivity.mCurrentUrl = BaseActivity.MY_TRANSACTIONS_URL + URL_EXTENTIONS;
                mContext.ChangeMenuIcon();
                mContext.replaceFragmentOpen(new WebViewFragment());

                break;

            case R.id.ll_my_password:

                BaseActivity.mCurrentUrl = BaseActivity.MY_PASSWORD_URL + URL_EXTENTIONS;
                mContext.ChangeMenuIcon();
                mContext.replaceFragmentOpen(new WebViewFragment());

                break;

            case R.id.ll_cashflow:

                BaseActivity.mCurrentUrl = BaseActivity.MY_CASH_FLOW_URL + URL_EXTENTIONS;
                mContext.ChangeMenuIcon();
                mContext.replaceFragmentOpen(new WebViewFragment());

                break;

            case R.id.ll_withdraw:

                BaseActivity.mCurrentUrl = BaseActivity.MY_WITHDRAW_URL + URL_EXTENTIONS;
                mContext.ChangeMenuIcon();
                mContext.replaceFragmentOpen(new WebViewFragment());

                break;

            case R.id.ll_take_a_break:

                BaseActivity.mCurrentUrl = BaseActivity.MY_TAKE_A_BREAK_URL + URL_EXTENTIONS;
                mContext.ChangeMenuIcon();
                mContext.replaceFragmentOpen(new WebViewFragment());

                break;

            case R.id.ll_responsible_gambling:

                BaseActivity.mCurrentUrl = BaseActivity.MY_RESPONSIBLE_GAMBLING + URL_EXTENTIONS;
                mContext.ChangeMenuIcon();
                mContext.replaceFragmentOpen(new WebViewFragment());

                break;

            case R.id.ll_logout:

//                BaseActivity.mIsLogOut = true;

//                https://cssupporter.azurewebsites.net?logout=[userid]
                BaseActivity.mCurrentUrl = BaseActivity.WEBSITE_URL + "logout="+BaseActivity.mUserID;
                mContext.ChangeMenuIcon();
                mContext.replaceFragmentOpen(new WebViewFragment());


                break;

        }

    }


    private void DepositeBalance() {

        if (TextUtils.isEmpty(edt_nav_amount.getText().toString().trim())) {
            //write your code here
            Toast.makeText(mContext, "Please ensure you have entered amount.", Toast.LENGTH_LONG).show();
        } else if (TextUtils.isEmpty(edt_nav_cvs.getText().toString().trim())) {
            Toast.makeText(mContext, "Please ensure you have entered cv2.", Toast.LENGTH_LONG).show();
        } else {
            sendUpdtaeBalanceRequest();
        }
    }

    private void sendUpdtaeBalanceRequest() {
        mContext.showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

//        String BearerToken = "Bearer Dr6Yv8XLHzDqwdu23mia6jh4bqZfaiOTNH5YjxWZLdfZshW60HS-0-TM4QVEeOZVaSrJxWvLAd3lpyBI3elXoBrRJgtDgSEx6MU2tQBqNkOFpWSJ86to9Otia6rBP9QkcVKt96VdMuOOVL6GuQuHv6eyFViIkMz7lF1Cj5ssdHujHF4GbV9g-1BTZAvCAQuPKEDAHYhsG-OuABAZfK4XMbyflPqpfbpnbKgBnfqh4YNTHGCaO4xqPhfpSMR1Z2xxZbEy4rAFSqu41z3VtT50BqhCSyLUcfyF2UyN9iHddRCL02--7pzlOrNA3MZtnx1_nXCNWUXd6kh6iOO5CFmHmjMMr_269-4Q9PTmPqRQs3g";

        String BearerToken = "Bearer " + BaseActivity.mAccessToken;
        String cv2 = edt_nav_cvs.getText().toString();
        String UserId = BaseActivity.mUserID;
        String amount = edt_nav_amount.getText().toString();


        Call<UpdateBalance> call = service.sendUpdateBalanceRequest(cv2, UserId, amount, BearerToken);

        call.enqueue(new Callback<UpdateBalance>() {
            @Override
            public void onResponse(Call<UpdateBalance> call, Response<UpdateBalance> response) {
                mContext.showWaitIndicator(false);
                try {
                    if (response.code() == 200) {

                        if (response.body().getBalance() != null) {

                            Log.d("CLUB_NAME_RESPONSE", "response code 200 : " + response.body().getBalance());

                            BaseActivity.mBalance = Double.parseDouble(response.body().getBalance().toString());
//                            BaseActivity.mBalance = Integer.parseInt(response.body().getBalance().toString());

                            NumberFormat mNumberFormat = NumberFormat.getInstance();
                            mNumberFormat.setMinimumFractionDigits(2);
                            mNumberFormat.setMaximumFractionDigits(2);
                            String Balance = mNumberFormat.format(BaseActivity.mBalance);

                            mContext.UpdtaeBalanceLable();
//                            txt_update_balance.setText("Quick Deposit: " + BaseActivity.mCurrencySymbol + Balance);
                            Toast.makeText(mContext, "Your balance successfully updated.", Toast.LENGTH_SHORT).show();
                            edt_nav_cvs.setText("");
                            edt_nav_amount.setText("");

                        } else {

//                            Toast.makeText(mContext, "Club Code not Found", Toast.LENGTH_LONG).show();
                        }


                    } else if (response.code() == 400) {

                        Gson gson = new GsonBuilder().create();
                        ClubCodeError mError = new ClubCodeError();
                        try {
                            mError = gson.fromJson(response.errorBody().string(), ClubCodeError.class);

                            Log.d("CLUB_NAME_RESPONSE", "response code 400 : " + mError.getMessage());
                            Toast.makeText(mContext, "" + mError.getMessage(), Toast.LENGTH_LONG).show();


                        } catch (Exception e) {
                            // handle failure to read error
                            Log.e("CLUB_NAME_RESPONSE", "response code 400 Exception : " + e);
                        }

                    }


                } catch (Exception e) {
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<UpdateBalance> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d("onFailure", t.toString());

            }
        });



    }

    public void setCustomDrawable(int backgroundColor, int borderColor) {
        int radius = 20;
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setCornerRadii(new float[]{radius, radius, radius, radius, radius, radius, radius, radius});
        shape.setColor(backgroundColor);
//        shape.setStroke(3, borderColor);
        btn_deposit.setBackground(shape);
    }

}
