package com.sp.clubsupporter.adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sp.clubsupporter.R;
import com.sp.clubsupporter.activity.DashBoardActivity;
import com.sp.clubsupporter.common.BaseActivity;
import com.sp.clubsupporter.models.MyClubDisplay;
import com.sp.clubsupporter.utility.TypeFaces;

import java.util.ArrayList;

/**
 * Created by My 7 on 22-Aug-18.
 */

public class MyClubDisplayAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    ArrayList<MyClubDisplay> mMyClubDisplayList = new ArrayList<>();

    DashBoardActivity mContext;

    public MyClubDisplayAdapter(DashBoardActivity mContext, ArrayList<MyClubDisplay> list)
    {
        this.mContext = mContext;
        mMyClubDisplayList = list;
    }

    @Override
    public TopSearchesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_my_club_item, null);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        itemView.setLayoutParams(lp);

        return new TopSearchesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        MyClubDisplay data = mMyClubDisplayList.get(position);
//        MyViewHolder mMyViewHolder = new MyViewHolder();
        TopSearchesViewHolder mMyViewHolder= (TopSearchesViewHolder) holder;

        /*if (position == 0){
            mMyViewHolder.txt_item_top_searches_title.setTextColor(ContextCompat.getColor(mContext, R.color.concentrates));
        }else {
            mMyViewHolder.txt_item_top_searches_title.setTextColor(ContextCompat.getColor(mContext, R.color.black));
        }*/

        mMyViewHolder.txt_myclub.setText(data.getName()+" ("+data.getLiveGames()+")");
        mMyViewHolder.txt_myclub.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));

        mMyViewHolder.icon_myclub.setTextColor(Color.parseColor(BaseActivity.LINK_COLOR));

        mMyViewHolder.icon_myclub.setTypeface(TypeFaces.getTypeFace(mContext, "fontawesome-webfont.ttf"));


        if(position == 0){

            mMyViewHolder.icon_myclub.setText(mContext.getString(R.string.nav_icon_signed_club));
        }else {

            mMyViewHolder.icon_myclub.setText(mContext.getString(R.string.nav_icon_club_exchange));
        }

//        if(data.isSignedUpClub()){
//
//            mMyViewHolder.icon_myclub.setText(mContext.getString(R.string.nav_icon_signed_club));
//
//        }else if(data.isCurrentClub()){
//
//            mMyViewHolder.icon_myclub.setText(mContext.getString(R.string.nav_icon_current_club));
//
//        }else {
//
//            mMyViewHolder.icon_myclub.setText(mContext.getString(R.string.nav_icon_club_exchange));
//
//        }
    }

    @Override
    public int getItemCount() {
        return mMyClubDisplayList.size();
    }

    public class TopSearchesViewHolder extends RecyclerView.ViewHolder {

        LinearLayout ll_main_my_club ;

        TextView txt_myclub, icon_myclub;

        public TopSearchesViewHolder(View itemView) {
            super(itemView);

            ll_main_my_club = itemView.findViewById(R.id.ll_main_my_club);
            txt_myclub = itemView.findViewById(R.id.txt_myclub);
            icon_myclub = itemView.findViewById(R.id.icon_myclub);

        }
    }
}
