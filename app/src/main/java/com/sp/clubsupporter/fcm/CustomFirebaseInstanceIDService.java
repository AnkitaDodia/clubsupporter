package com.sp.clubsupporter.fcm;

/**
 * Created by Adite-Ankita on 23-Aug-16.
 */

import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.sp.clubsupporter.common.BaseActivity;

public class CustomFirebaseInstanceIDService extends FirebaseInstanceIdService
{
    private static final String TAG = CustomFirebaseInstanceIDService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        BaseActivity.FCMTOKEN = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG, "Token_Value: " + BaseActivity.FCMTOKEN);

        saveFCMID(BaseActivity.FCMTOKEN);

//        sendFCMTokenRequest();
    }

   /* @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.

        BaseActivity.FCMTOKEN = token;
        BaseActivity.FCMTOKEN = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG, "Token_Value: " + BaseActivity.FCMTOKEN);

//        saveFCMID(BaseActivity.FCMTOKEN);
    }*/

    public void saveFCMID(String fcm_token) {
        SharedPreferences sp = getSharedPreferences("FCM", MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putString("FCM_ID", fcm_token);
        spe.commit();
    }



//    private void sendFCMTokenRequest() {
//        try {
////            showWaitIndicator(true);
//
//            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();
//
//            RestInterface restInterface = adapter.create(RestInterface.class);
//
//            restInterface.sendFCMToken(BaseActivity.refreshedToken, new Callback<FCM>() {
//                @Override
//                public void success(FCM model, Response response) {
////                    showWaitIndicator(false);
//
//                    if (response.getStatus() == 200) {
//                        try {
//                            Log.e("FCM_TOKEN_RESPONSE", "" + new Gson().toJson(model));
//
//
//
//                        } catch (Exception e) {
//
//                            e.printStackTrace();
//                        }
//                    }
//                }
//
//                @Override
//                public void failure(RetrofitError error) {
//
////                    showWaitIndicator(false);
//
//                    Log.e("ERROR", "" + error.getMessage());
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}