package com.sp.clubsupporter.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sp.clubsupporter.R;
import com.sp.clubsupporter.common.BaseActivity;
import com.sp.clubsupporter.fragment.BurgerMenuFragment;
import com.sp.clubsupporter.fragment.WebViewFragment;
import com.sp.clubsupporter.models.Balance;
import com.sp.clubsupporter.models.ClubCodeError;
import com.sp.clubsupporter.models.ForgotPassword;
import com.sp.clubsupporter.models.MyClubDisplay;
import com.sp.clubsupporter.models.SupporterRole;
import com.sp.clubsupporter.models.WhiteLablling;
import com.sp.clubsupporter.restinterface.RestInterface;

import java.text.NumberFormat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DashBoardActivity extends BaseActivity {

    Context mContext;


    FrameLayout fl_fragment_container;
    LinearLayout ll_top_bar, ll_balance_container, ll_nav_menu, ll_home_nav;
    Fragment currentFragment;
    ImageView img_club_logo, img_nav_icon;
    Boolean isNavFrgmentOpen = false;
    TextView txt_top_balance;

    String Default_ICON = "https://cssupporter.azurewebsites.net/framework/images/navcrest.png";
    String URL_EXTENTIONS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);

        mContext = this;

        initViews();
        setViewListner();

        /*mClubID = getClubId();
        mUserID = getUserId();
        mEmailID = getEmailId();
        mAccessToken = getLoginToken();

        if (BaseActivity.mSwitchClub){

            Log.e("mSwitchClub","with Clubid  : "+BaseActivity.mSwitchClub);
            URL_EXTENTIONS = "clubid=" + mClubID;


        }else {
            Log.e("mSwitchClub","with id and emailid  : "+BaseActivity.mSwitchClub);
            URL_EXTENTIONS = "id=" + mUserID + "&email=" + mEmailID;
        }

        BaseActivity.mCurrentUrl = BaseActivity.WEBSITE_URL + URL_EXTENTIONS;*/

//        Toast.makeText(mContext, "Check Freeze issue :"+BaseActivity.mCurrentUrl, Toast.LENGTH_LONG).show();


//        replaceFragmentClose(new WebViewFragment());
//        sendWhiteLablingRequest();

//        Dark colour and navigation colour is #053B45
//        Link colour in navigation is #FFFFFF
//        Top navigation bar has a white background and the balance container has a background colour of #6AC1B2


    }

    private void initViews() {

        fl_fragment_container = findViewById(R.id.fl_fragment_container);
        ll_balance_container = findViewById(R.id.ll_balance_container);
        ll_nav_menu = findViewById(R.id.ll_nav_menu);
        ll_home_nav = findViewById(R.id.ll_home_nav);
        ll_top_bar = findViewById(R.id.ll_top_bar);

        img_club_logo = findViewById(R.id.img_club_logo);
        img_nav_icon = findViewById(R.id.img_nav_icon);

        txt_top_balance = findViewById(R.id.txt_top_balance);
    }

    private void setViewListner() {

        ll_nav_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //false
                if (isNavFrgmentOpen) {

                    img_nav_icon.setImageResource(R.drawable.ic_nav_menu);
                    replaceFragmentOpen(new WebViewFragment());

                } else {


                    img_nav_icon.setImageResource(R.drawable.ic_close);
                    replaceFragmentOpen(new BurgerMenuFragment());

                }
            }
        });

        ll_home_nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String URL_EXTENTIONS = "id=" + mUserID + "&email=" + mEmailID;
                ChangeMenuIcon();
                BaseActivity.mCurrentUrl = BaseActivity.GAMES_DASHBOARD_URL + URL_EXTENTIONS;
                replaceFragment(new WebViewFragment());
            }
        });

        ll_balance_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sendRefreshBalanceRequest();
            }
        });


    }

    public void ChangeMenuIcon() {

        img_nav_icon.setImageResource(R.drawable.ic_nav_menu);
    }

    public void replaceFragment(Fragment targetFragment) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fl_fragment_container, targetFragment, targetFragment.getClass().getName());
        transaction.addToBackStack(targetFragment.getClass().getName());
//        transaction.commit();
        transaction.commitAllowingStateLoss();

        currentFragment = targetFragment;
    }

    public void replaceFragmentOpen(Fragment targetFragment) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.sliding_in_right, R.anim.sliding_out_right) // Top Fragment Animation
                .show(new BurgerMenuFragment())
                .setCustomAnimations(R.anim.sliding_in_right, R.anim.sliding_out_right) // Bottom Fragment Animation
                .show(new WebViewFragment());
        transaction.replace(R.id.fl_fragment_container, targetFragment, targetFragment.getClass().getName());
        transaction.addToBackStack(targetFragment.getClass().getName());
//        transaction.commit();
        transaction.commitAllowingStateLoss();

        currentFragment = targetFragment;
        isNavFrgmentOpen = !isNavFrgmentOpen;
    }

    public void replaceFragmentClose(Fragment targetFragment) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.sliding_out_right, R.anim.sliding_in_right) // Top Fragment Animation
                .show(new BurgerMenuFragment())
                .setCustomAnimations(R.anim.sliding_in_right, R.anim.sliding_out_right) // Bottom Fragment Animation
                .show(new WebViewFragment());
        transaction.replace(R.id.fl_fragment_container, targetFragment, targetFragment.getClass().getName());
        transaction.addToBackStack(targetFragment.getClass().getName());
//        transaction.commit();
        transaction.commitAllowingStateLoss();

        currentFragment = targetFragment;

    }

    public void sendWhiteLablingRequest() {
//        showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        String UserName = BaseActivity.mEmailID;
//        mAccessToken = "XPf8uEqtpxiwgipL0C3XiTbC9IAdO-lpiGiDGel1VTD1uxhju7PnW1UISdl5wOXT6dY8zec42nrn7Tk3E4kIod849_0X-xsnQH4iI7GeIvXw75gjd4hX5VbDSrExHbZEppooU63Aqyg4JJQjqDNuTl1lU9Hj92YMJkAj1lpDloN8VSTdrLemHQVrvkhbtLF-O1O7Rb6anAix2XCZFm_yaOHfhRb6PdRZR4lN3sL9Sr9t57eNSzdJlhuMtkGZf9kVW1SmjZmBgRl0uWnGKSITtK6jFnqkjfhQKzauz81i4aeAoNRcAnLWY6zuDIw0x-NUvOrndFElIWuMvF_SsPv90EeIDBiqFQY32Bg8iBCOXwuQlb4b2G0z69GdELhDD00ubY6d4ihXLR-jyHN4H5ErUg";
        String BearerToken = "Bearer " + mAccessToken;

        Log.e("WHITE_LABLING", "Befor Call : UserName   " + BaseActivity.mEmailID);
        Log.e("WHITE_LABLING", "Befor Call : clubid   " + mClubID);
        Log.e("WHITE_LABLING", "Befor Call : Bearer Token   " + mAccessToken);

//        String BearerToken = "Bearer 9thAyL7MLBetrJ3XXDcU56EIUvkJ-sXXqEoGWhjoYKNtIq5LJaC9WsFlEM42mIpfk68Ha_aFv628b6F2KZydRDcAkHv8Vvmc081pycbiyK8vho-8CpjbhM2EEFoUyZJp27EAvBkKtS5X1p2Y-zbIgQr5EvT8QKeLTz1Mvj1kFC_maMpS5fQfy8sk3GoJH0Q__KIAui8Oe6yH88LS4Xxt2qCIttIn1V6_ItrtNlqCMiyrggcZAev2WWONTAHWxtqFz1vj73fvDzh0CDuplTYhNCuiuHinjiOQlBp7H3WwzlQaqJt1VHi5v43rgwawYeZ_REIk4BfbKqXWO5K3kRrkfgWXCkr2SD4q5Br3iBx9Lhw";

        Call<WhiteLablling> call = service.sendWhiteLablingRequest(UserName, mClubID, BearerToken);

        call.enqueue(new Callback<WhiteLablling>() {
            @Override
            public void onResponse(Call<WhiteLablling> call, Response<WhiteLablling> response) {
//                showWaitIndicator(false);
                try {
                    if (response.code() == 200) {

                        Log.d("WHITE_LABLING", "RESPONSE code 200 : " + response.body().getDisableStyling().booleanValue());

                        if (response.body().getDisableStyling() != null) {
//                            Log.d("WHITE_LABLING", "RESPONSE code 200 : " + response.body().getDisableStyling().booleanValue());


                            if (response.body().getDisableStyling().booleanValue() == true) {

                                Glide.with(mContext)
                                        .load(Default_ICON)
                                        .into(img_club_logo);

                                LINK_COLOR = "#FFFFFF";
                                NAVIGATION_COLOR = "#053B45";
                                TOP_BAR_COLOR = "#FFFFFF";
                                CONTAINER_COLOR = "#6AC1B2";

                            } else {

                                Log.e("WHITE_LABLING", "getHeaderColour : " + response.body().getHeaderColour());
                                Log.e("WHITE_LABLING", "getLinkColour : " + response.body().getLinkColour());
                                Log.e("WHITE_LABLING", "getDarkColor : " + response.body().getDarkColor());
                                Log.e("WHITE_LABLING", "getNavigationColour : " + response.body().getNavigationColour());


                                LINK_COLOR = response.body().getLinkColour();
                                NAVIGATION_COLOR = response.body().getNavigationColour();
                                TOP_BAR_COLOR = response.body().getHeaderColour();
                                CONTAINER_COLOR = response.body().getDarkColor();


                                Glide.with(mContext)
                                        .load(response.body().getCrest())
                                        .into(img_club_logo);
                            }

                            setCustomDrawable(Color.parseColor(BaseActivity.CONTAINER_COLOR), Color.BLUE);
                            ll_top_bar.setBackgroundColor(Color.parseColor(BaseActivity.TOP_BAR_COLOR));
                            img_nav_icon.setColorFilter(Color.parseColor(BaseActivity.CONTAINER_COLOR), android.graphics.PorterDuff.Mode.MULTIPLY);

                            ll_top_bar.setVisibility(View.VISIBLE);

                        } else {

                            Toast.makeText(mContext, "Club Code not Found", Toast.LENGTH_LONG).show();
                        }

                        sendSupporterRoleRequest();

                    } else if (response.code() == 400) {

                        Gson gson = new GsonBuilder().create();
                        ClubCodeError mError = new ClubCodeError();
                        try {
                            mError = gson.fromJson(response.errorBody().string(), ClubCodeError.class);

                            Log.d("WHITE_LABLING", "response code 400 : " + mError.getMessage());


                        } catch (Exception e) {
                            // handle failure to read error
                            Log.e("WHITE_LABLING", "response code 400 Exception : " + e);
                        }

                    } else if (response.code() == 401) {

                        Gson gson = new GsonBuilder().create();
                        ClubCodeError mError = new ClubCodeError();
                        try {
                            mError = gson.fromJson(response.errorBody().string(), ClubCodeError.class);

                            Log.d("WHITE_LABLING", "response code 400 : " + mError.getMessage());
//                            Toast.makeText(mContext, ""+mError.getMessage(), Toast.LENGTH_LONG).show();

                            setLogin(0);

                            Intent i = new Intent(mContext, LoginActivity.class);
                            startActivity(i);
                            finish();


                        } catch (Exception e) {
                            // handle failure to read error
                            Log.e("WHITE_LABLING", "response code 400 Exception : " + e);
                        }

                    }





                } catch (Exception e) {
//                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WhiteLablling> call, Throwable t) {
//                showWaitIndicator(false);
                Log.d("onFailure", t.toString());

            }
        });

    }

    public void sendSupporterRoleRequest() {
//        showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        String UserName = mEmailID;
        String BearerToken = "Bearer " + mAccessToken;

        Log.e("SUPPORTER_ROLE", "mEmailID : " + mEmailID);
        Log.e("SUPPORTER_ROLE", "mAccessToken : " + mAccessToken);
        Log.e("SUPPORTER_ROLE", "mClubID : " + mClubID);

        Call<SupporterRole> call = service.sendSupporterRoleRequest(UserName, mClubID, BearerToken);

        call.enqueue(new Callback<SupporterRole>() {
            @Override
            public void onResponse(Call<SupporterRole> call, Response<SupporterRole> response) {
//                showWaitIndicator(false);
                try {


                    if (response.code() == 200) {

                        Log.e("SUPPORTER_ROLE", "RESPONSE code 200 : " + response.body().getBalance());

                        Log.d("SUPPORTER_ROLE", "Check TempPassword : " + response.body().getTempPassword());
                        mIsPasswordChange = response.body().getTempPassword();

                        if (response.body().getBalance() != null) {

                            Log.e("SUPPORTER_ROLE", "RESPONSE code 200 : " + response.body().getClubName().getCurrency());
                            Log.e("SUPPORTER_ROLE", "RESPONSE code 200 : " + response.body().getBalance());

                            mCurrencySymbol = response.body().getClubName().getCurrency();
                            mBalance = response.body().getBalance();
                            mLastLoginDate = response.body().getLastLoginDate();

                            UpdtaeBalanceLable();

                            mMyClubDisplayList.clear();

                            for (int i = 0; i < response.body().getMyClubs().size(); i++) {

                                MyClubDisplay mMyClubDisplay = new MyClubDisplay();

                                mMyClubDisplay.setName(response.body().getMyClubs().get(i).getClub().getName());
                                mMyClubDisplay.setClubId(response.body().getMyClubs().get(i).getClub().getClubId());
                                mMyClubDisplay.setLiveGames(response.body().getMyClubs().get(i).getLiveGames());



                                Log.e("SUPPORTER_ROLE", "response code 200 : " + response.body().getMyClubs().get(i).getClub().getClubId());
                                Log.e("SUPPORTER_ROLE", "response code 200 : " + response.body().getMyClubs().get(i).getClub().getName());
                                Log.e("SUPPORTER_ROLE", "response code 200 : " + response.body().getMyClubs().get(i).getLiveGames());

                                mMyClubDisplayList.add(mMyClubDisplay);
                            }


//                            Toast.makeText(mContext, "Club Code not Found", Toast.LENGTH_LONG).show();
                        } else {


                            Log.d("SUPPORTER_ROLE", "Else Balance : " + response.body().getBalance());
//                            Toast.makeText(mContext, "Club Code not Found", Toast.LENGTH_LONG).show();
                        }

                        replaceFragmentClose(new WebViewFragment());


                    } else if (response.code() == 400) {

                        Gson gson = new GsonBuilder().create();
                        ClubCodeError mError = new ClubCodeError();
                        try {
                            mError = gson.fromJson(response.errorBody().string(), ClubCodeError.class);

                            Log.d("SUPPORTER_ROLE", "response code 400 : " + mError.getMessage());


                        } catch (Exception e) {
                            // handle failure to read error
                            Log.e("SUPPORTER_ROLE", "response code 400 Exception : " + e);
                        }

                    }


                } catch (Exception e) {
//                    showWaitIndicator(false);
                    Log.e("SUPPORTER_ROLE", "response code 400 Exception : " + e);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SupporterRole> call, Throwable t) {
//                showWaitIndicator(false);
                Log.d("onFailure", t.toString());

            }
        });

    }

    private void sendRefreshBalanceRequest() {

        showWaitIndicator(true);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<Balance> call = service.sendRefreshBalanceRequest(BaseActivity.mUserID);

        call.enqueue(new Callback<Balance>() {
            @Override
            public void onResponse(Call<Balance> call, Response<Balance> response) {

                showWaitIndicator(false);

                Log.d("REFRESH_BALANCE", "RESPONSE code : " + response.code());

                try {
                    if (response.code() == 200) {

                        if (response.body().getBalance() != null) {

                            Log.d("REFRESH_BALANCE", "RESPONSE code 200 : " + response.body().getBalance());

                            mBalance = response.body().getBalance();

//                            NumberFormat mNumberFormat = NumberFormat.getInstance();
//                            mNumberFormat.setMinimumFractionDigits(2);
//                            mNumberFormat.setMaximumFractionDigits(2);
//                            mBalance = response.body().getBalance();
                            UpdtaeBalanceLable();
                        }


                    } else if (response.code() == 400) {


                        Gson gson = new GsonBuilder().create();
                        ForgotPassword mError = new ForgotPassword();
                        try {
                            mError = gson.fromJson(response.errorBody().string(), ForgotPassword.class);

                            Log.e("REFRESH_BALANCE", "response code 400 : " + mError.getMessage());
                            Toast.makeText(mContext, ""+mError.getMessage(), Toast.LENGTH_SHORT).show();


                        } catch (Exception e) {
                            // handle failure to read error
                            Log.e("REFRESH_BALANCE", "response code 400 Exception : " + e);
                        }

                    }


                } catch (Exception e) {
                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Balance> call, Throwable t) {
                showWaitIndicator(false);
                Log.d("onFailure", t.toString());

            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        mClubID = getClubId();
        mUserID = getUserId();
        mEmailID = getEmailId();
        mAccessToken = getLoginToken();

        if (BaseActivity.mSwitchClub){

            Log.e("mSwitchClub","with Clubid  : "+BaseActivity.mSwitchClub);
            URL_EXTENTIONS = "clubid=" + mClubID;

        }else {

            Log.e("mSwitchClub","with id and emailid  : "+BaseActivity.mSwitchClub);
            URL_EXTENTIONS = "id=" + mUserID + "&email=" + mEmailID;
        }

        BaseActivity.mCurrentUrl = BaseActivity.WEBSITE_URL + URL_EXTENTIONS;


        sendWhiteLablingRequest();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    public void UpdtaeBalanceLable() {

        String Balance;

        if (mBalance == 0) {

            Balance = "0.00";

        } else {

            NumberFormat mNumberFormat = NumberFormat.getInstance();
            mNumberFormat.setMinimumFractionDigits(2);
            mNumberFormat.setMaximumFractionDigits(2);
            Balance = mNumberFormat.format(mBalance);

        }


        txt_top_balance.setText("Balance: " + BaseActivity.mCurrencySymbol + Balance);
//        txt_top_balance.setText("Balance: " + Balance + " " + mCurrencySymbol);
    }

    public void setCustomDrawable(int backgroundColor, int borderColor) {
        int radius = 30;
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setCornerRadii(new float[]{radius, radius, radius, radius, 0, 0, 0, 0});
        shape.setColor(backgroundColor);
//        shape.setStroke(3, borderColor);
        ll_balance_container.setBackground(shape);
    }

}
