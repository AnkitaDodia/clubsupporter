package com.sp.clubsupporter.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.MediaController;

import com.bumptech.glide.Glide;
import com.sp.clubsupporter.R;
import com.sp.clubsupporter.common.BaseActivity;

import pl.droidsonroids.gif.AnimationListener;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;


public class SplashActivity extends BaseActivity {

    Context mContext;
    ImageView img_splash;
//    private static int SPLASH_TIME_OUT = 9000;

    GifImageView img_splash_gif;
    GifDrawable gifFromResource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mContext = this;

        img_splash = findViewById(R.id.img_splash);
        img_splash_gif = findViewById(R.id.img_splash_gif);

        loadImageGif();

//        new Handler().postDelayed(new Runnable() {
//
//            @Override
//            public void run() {
//
//                Intent i = new Intent(mContext, LoginActivity.class);
//                startActivity(i);
//                // close this activity
//                finish();
//
//            }
//        }, SPLASH_TIME_OUT);

    }

    private void loadImageGif() {

//        Glide.with(mContext)
//                .asGif()
//                .load(R.drawable.splash_loading)
//                .into(img_splash);
//                .placeholder(R.drawable.loading2);

        try{

            gifFromResource = new GifDrawable( getResources(), R.drawable.splash_loading2);
            gifFromResource.setLoopCount(1);
            img_splash_gif.setImageDrawable(gifFromResource);

            gifFromResource.addAnimationListener(new AnimationListener() {
                @Override
                public void onAnimationCompleted(int loopNumber) {

                    gifFromResource.stop();
                    onGifFinished();
                }
            });

//            MediaController mc = new MediaController(this);
//            mc.setMediaPlayer((GifDrawable) img_splash_gif.getDrawable());
//            gifFromResource.setm



        }catch (Exception e){

        }

    }

    private void onGifFinished(){

        if(getLogin() == 1){

            mUserID = getUserId();
            mAccessToken = getLoginToken();
            mEmailID = getEmailId();
            mClubID = getClubId();

            Log.e("LOGIN_DATA", "mUserID : "+mUserID);
            Log.e("LOGIN_DATA", "mAccessToken : "+mAccessToken);
            Log.e("LOGIN_DATA", "mEmailID : "+mEmailID);
            Log.e("LOGIN_DATA", "mClubID : "+mClubID);



            BaseActivity.mSwitchClub = false;

            Intent i = new Intent(mContext, DashBoardActivity.class);
            startActivity(i);
            // close this activity
            finish();

        }else {

            Intent i = new Intent(mContext, LoginActivity.class);
            startActivity(i);
            // close this activity
            finish();
        }

    }
}
