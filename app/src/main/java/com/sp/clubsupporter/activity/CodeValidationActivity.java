package com.sp.clubsupporter.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sp.clubsupporter.R;
import com.sp.clubsupporter.common.BaseActivity;
import com.sp.clubsupporter.models.ClubCode;
import com.sp.clubsupporter.models.ClubCodeError;
import com.sp.clubsupporter.models.FoundClubCode;
import com.sp.clubsupporter.models.FoundClubName;
import com.sp.clubsupporter.restinterface.RestInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CodeValidationActivity extends BaseActivity implements OnItemSelectedListener {

    Context mContext;
    EditText edt_club_code;
    Button btn_next;
    TextView txt_code_validation_login, txt_clubname;
    Spinner spnr_team_name;
    LinearLayout ll_spinner_holder;

    AutoCompleteTextView auto_txt_club_name;

    List<String> categories = new ArrayList<String>();
    List<String> FoundClubNameList = new ArrayList<String>();

    private String TAG = "CODE_VALIDATION";
    private boolean isInternet, isClubCodeEmpty = true, isClubNameEmpty = true, isSearchModeCode;
    private String club_name_query;

    NestedScrollView ns_code_validation_parent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_code_validation);

        getSupportActionBar().hide();

        mContext = this;

        initviews();
        setListners();

//        registration_level = 1 ;

//        if(registration_level == 1){
//
//            edt_club_code.setText(CLUB_CODE);
////            edt_club_name.setText(club_name);
//        }
    }

    private void initviews() {

        edt_club_code = findViewById(R.id.edt_club_code);

        ll_spinner_holder = findViewById(R.id.ll_spinner_holder);

        spnr_team_name = findViewById(R.id.spnr_team_name);

        txt_code_validation_login = findViewById(R.id.txt_code_validation_login);
        txt_clubname = findViewById(R.id.txt_clubname);

        btn_next = findViewById(R.id.btn_next);

        auto_txt_club_name = findViewById(R.id.auto_txt_club_name);

        ns_code_validation_parent = findViewById(R.id.ns_code_validation_parent);


    }

    private void setListners() {


        txt_code_validation_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(mContext, LoginActivity.class);
                startActivity(i);
                finish();

            }
        });

        edt_club_code.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus)
                {

                    auto_txt_club_name.setText("");
                    isSearchModeCode = true;
//                    Toast.makeText(mContext, "Click on club code", Toast.LENGTH_SHORT).show();
                }
            }
        });

        auto_txt_club_name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus)
                {
                    edt_club_code.setText("");
                    isSearchModeCode = false;
                    auto_txt_club_name.showDropDown();
//                    Toast.makeText(mContext, "Click on club name", Toast.LENGTH_SHORT).show();
                }
            }
        });

        auto_txt_club_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable mEditable) {

                if(TextUtils.isEmpty(mEditable.toString().trim())){

                    ll_spinner_holder.setVisibility(View.GONE);
                    txt_clubname.setVisibility(View.GONE);

                    isClubCodeEmpty = false;
                    isClubNameEmpty = true;

                }else {


                    CLUB_CODE = null;

                    sendFoundClubCodeRequest();

                    isClubCodeEmpty = false;
                    isClubNameEmpty = false;
                }

                EnableButton(isClubCodeEmpty, isClubNameEmpty);
            }
        });

        auto_txt_club_name.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                CLUB_NAME = parent.getItemAtPosition(position).toString();
                Log.e(TAG, "Selected ClubName: "+CLUB_NAME);

                View mView = getCurrentFocus();
                if (mView != null) {
                    InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(view.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                }

                sendClubNameRequest();

            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e(TAG, "isSearchModeCode : "+isSearchModeCode);



                if(CLUB_CODE == null){


                    if(isSearchModeCode){


//                        Intent i = new Intent(mContext, RegistrationActivity.class);
//                        startActivity(i);

                        sendCodeValidationRequest();



                    }
//                    else {
//
//                        if(edt_club_code.getText().toString().length() == 0) {
//
//                            Toast.makeText(mContext, "Enter your club code to start your registration.", Toast.LENGTH_LONG).show();
//
//                        }else {
//                            sendCodeValidationRequest();
//                        }
//                    }

                }else {

                    Intent i = new Intent(mContext, RegistrationActivity.class);
                    startActivity(i);

                }

            }
        });

        edt_club_code.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable mEditable) {

                if(TextUtils.isEmpty(mEditable.toString().trim())){
                    //write your code here
                    CLUB_CODE = null;
//                    spnr_team_name.setVisibility(View.GONE);
                    ll_spinner_holder.setVisibility(View.GONE);
                    txt_clubname.setVisibility(View.GONE);

                    isClubCodeEmpty = true;

                }else {

                    isClubCodeEmpty = false;
                }

                EnableButton(isClubCodeEmpty, isClubNameEmpty);
            }
        });


        spnr_team_name.setOnItemSelectedListener(this);

    }

    private void sendCodeValidationRequest() {

        showWaitIndicator(true);


        Log.e(TAG, "isSearchModeCode Before Request: "+isSearchModeCode);

        if (isSearchModeCode) {

//            Log.e(TAG, "CLUB_CODE Before Request: "+CLUB_CODE);
            CLUB_CODE = edt_club_code.getText().toString();
            auto_txt_club_name.setText("");

        } else {


        }

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Log.e(TAG, "CLUB_CODE Before Request: "+CLUB_CODE);
        Call<ClubCode> call = service.sendCodeValidationRequest(CLUB_CODE);

        call.enqueue(new Callback<ClubCode>() {
            @Override
            public void onResponse(Call<ClubCode> call, Response<ClubCode> response) {

                showWaitIndicator(false);
                try {
                    if (response.code() == 200) {



                        txt_clubname.setVisibility(View.VISIBLE);
                        txt_clubname.setText(""+response.body().getClubName());
                        CLUB_NAME = response.body().getClubName();
                        categories.clear();

                        try {

                            if(response.body().getTeams().size() > 0){

                                for (int i = 0; i < response.body().getTeams().size(); i++) {

//                            response.body().getTeams().get(i).getTeamName();
                                    Log.d("CLUB_CODE_RESPONSE", "response code 200 : " + response.body().getTeams().get(i).getTeamName());
                                    categories.add(response.body().getTeams().get(i).getTeamName());
                                }

                                categories.add("Optional");
                                ll_spinner_holder.setVisibility(View.VISIBLE);
                                CreateTeamNameSppiner();
                            }

                        }catch (Exception e){


                            Intent i = new Intent(mContext, RegistrationActivity.class);
                            startActivity(i);

//                            Toast.makeText(mContext, "No Team Found", Toast.LENGTH_SHORT).show();
                        }



                    } else if (response.code() == 400) {

                        Gson gson = new GsonBuilder().create();
                        ClubCodeError mError = new ClubCodeError();
                        try {
                            mError = gson.fromJson(response.errorBody().string(), ClubCodeError.class);

                            Log.d("CLUB_CODE_RESPONSE", "response code 400 : " + mError.getMessage());


                        } catch (Exception e) {
                            // handle failure to read error
                            Log.e("CLUB_CODE_RESPONSE", "response code 400 Exception : " + e);
                        }

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    showWaitIndicator(false);
                }
            }

            @Override
            public void onFailure(Call<ClubCode> call, Throwable t) {
                showWaitIndicator(false);
                Log.d("onFailure", t.toString());

            }
        });

    }

    private void sendClubNameRequest() {
        showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<FoundClubCode> call = service.sendClubNameRequest(CLUB_NAME);

        call.enqueue(new Callback<FoundClubCode>() {
            @Override
            public void onResponse(Call<FoundClubCode> call, Response<FoundClubCode> response) {
                showWaitIndicator(false);
                try {
                    if (response.code() == 200) {

                        if (response.body().getCode() != null) {
                            Log.d("CLUB_NAME_RESPONSE", "response code 200 : " + response.body().getCode());

//                            edt_club_code.setText("" + response.body().getCode());
//                            edt_club_name.setEnabled(false);

                            CLUB_CODE = response.body().getCode();

                            sendCodeValidationRequest();

                        } else {

                            Toast.makeText(mContext, "Club Code not Found", Toast.LENGTH_LONG).show();
                        }


                    } else if (response.code() == 400) {

                        Gson gson = new GsonBuilder().create();
                        ClubCodeError mError = new ClubCodeError();
                        try {
                            mError = gson.fromJson(response.errorBody().string(), ClubCodeError.class);

                            Log.d("CLUB_NAME_RESPONSE", "response code 400 : " + mError.getMessage());


                        } catch (Exception e) {
                            // handle failure to read error
                            Log.e("CLUB_NAME_RESPONSE", "response code 400 Exception : " + e);
                        }

                    }


                } catch (Exception e) {
                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<FoundClubCode> call, Throwable t) {
                showWaitIndicator(false);
                Log.d("onFailure", t.toString());

            }
        });

    }

    private void sendFoundClubCodeRequest() {

//        showWaitIndicator(true);

        club_name_query = auto_txt_club_name.getText().toString();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<ArrayList<FoundClubName>> call = service.sendFoundClubnameRequest(club_name_query);

        call.enqueue(new Callback<ArrayList<FoundClubName>>() {
            @Override
            public void onResponse(Call<ArrayList<FoundClubName>> call, Response<ArrayList<FoundClubName>> response) {

//                showWaitIndicator(false);
                try {
                    if (response.code() == 200) {

                        Log.e("FOUND_CLUB_CODE", "RESPONSE code 200 : " + response.body().size());

                        FoundClubNameList.clear();


                        for (int i = 0; i < response.body().size(); i++) {

                            Log.d("FOUND_CLUB_CODE", "RESPONSE code 200 : " + response.body().get(i).getClubName());
                            FoundClubNameList.add(response.body().get(i).getClubName());
                        }


                        ArrayAdapter myAdapter = new ArrayAdapter<String>(mContext,
                                android.R.layout.simple_dropdown_item_1line, FoundClubNameList);
                        auto_txt_club_name.setAdapter(myAdapter);
                        auto_txt_club_name.showDropDown();

                        edt_club_code.setText("");

                    } else if (response.code() == 400) {

//                        Gson gson = new GsonBuilder().create();
//                        ClubCodeError mError = new ClubCodeError();
//                        try {
//                            mError = gson.fromJson(response.errorBody().string(), ClubCodeError.class);
//
//                            Log.d("RESPONSE", "response code 400 : " + mError.getMessage());
//
//
//                        } catch (Exception e) {
//                            // handle failure to read error
//                            Log.e("RESPONSE", "response code 400 Exception : " + e);
//                        }

                    }


                } catch (Exception e) {
                    e.printStackTrace();
//                    showWaitIndicator(false);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<FoundClubName>> call, Throwable t) {
//                showWaitIndicator(false);
                Log.d("onFailure", t.toString());

            }
        });

    }


    private void CreateTeamNameSppiner() {

        final int listsize = categories.size() - 1;
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, categories) {
            @Override
            public int getCount() {
                return(listsize); // Truncate the list
            }
        };
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnr_team_name.setAdapter(dataAdapter);

        spnr_team_name.setSelection(listsize);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        // On selecting a spinner item
        club_team = parent.getItemAtPosition(position).toString();
        // Showing selected spinner item
//        Toast.makeText(mContext, "Selected: " + item, Toast.LENGTH_LONG).show();

        if(club_team == "Optional"){

            club_team = null;
//            Toast.makeText(mContext,"Please select team.",Toast.LENGTH_LONG).show();
        }
    }

    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    private void EnableButton(boolean mIsClubCode, boolean mIsClubName){

        if(mIsClubCode && mIsClubName){

            btn_next.setBackgroundResource(R.drawable.btn_disable_selector);

        }else {

            btn_next.setBackgroundResource(R.drawable.btn_enable_selector);
        }

    }

    public static float dpToPx(Context context, float valueInDp) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }

}
