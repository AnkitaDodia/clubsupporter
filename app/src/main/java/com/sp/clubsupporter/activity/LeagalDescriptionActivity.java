package com.sp.clubsupporter.activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.sp.clubsupporter.R;
import com.sp.clubsupporter.common.BaseActivity;

public class LeagalDescriptionActivity extends BaseActivity {

    Context mContext;
    WebView webview_legal_description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leagal_description);

        mContext = this;

        webview_legal_description = findViewById(R.id.webview_legal_description);

        webview_legal_description.getSettings().setJavaScriptEnabled(true); // enable javascript

        webview_legal_description.setWebViewClient(new WebViewClient() {
            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(mContext, description, Toast.LENGTH_SHORT).show();
            }
            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
            }
        });


        if (getIntent().getScheme().equals("terms")) {

            mForLegal = "terms";
//            Toast.makeText(mContext, "onNewIntent terms", Toast.LENGTH_SHORT).show();
            webview_legal_description .loadUrl(BaseActivity.mLink_Terms_of_Use);

        }else if(getIntent().getScheme().equals("privacy")){

            mForLegal = "privacy";
//            Toast.makeText(mContext, "onNewIntent privacy", Toast.LENGTH_SHORT).show();

            webview_legal_description .loadUrl(BaseActivity.mLink_Privacy_policy);


        }


       /* if(mForLegal.equalsIgnoreCase("terms")){

            webview_legal_description .loadUrl(BaseActivity.mLink_Terms_of_Use);

        }else if(mForLegal.equalsIgnoreCase("privacy")){

            webview_legal_description .loadUrl(BaseActivity.mLink_Privacy_policy);

        }*/

    }
}
