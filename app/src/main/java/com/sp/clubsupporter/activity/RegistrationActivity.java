package com.sp.clubsupporter.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.util.Linkify;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sp.clubsupporter.R;
import com.sp.clubsupporter.common.BaseActivity;
import com.sp.clubsupporter.models.ForgotPassword;
import com.sp.clubsupporter.models.Registration;
import com.sp.clubsupporter.restinterface.RestInterface;
import com.sp.clubsupporter.utility.PasswordValidator;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegistrationActivity extends BaseActivity {

    Context mContext;

    LinearLayout ll_registration_first_part, ll_registration_second_part, ll_button_holder;
    EditText edt_first_name, edt_last_name, edt_email_address, edt_confirm_email_address,
            edt_nickname, edt_date_of_birth, edt_password, edt_confirm_password;

    CheckBox chbx_terms_and_privacy;

    Button btn_registration_back, btn_registration_next;
    TextView txt_registration_messages, txt_registration_team_name, txt_login, txt_legal_description;

    Calendar mCalender;
    private int mAge = 0;
    PasswordValidator mPasswordValidator;

    String mFirstname, mLastname, mEmail, mConfirm_email, mNickname, mDateofBirth, mPassword, mConfirm_password;
    boolean mAgree;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        getSupportActionBar().hide();

        mContext = this;

        SetViews();
        SetListners();

        registration_level = 1;
        mCalender = Calendar.getInstance();
        mPasswordValidator = new PasswordValidator();


    }

    private void SetViews() {

        ll_registration_first_part = findViewById(R.id.ll_registration_first_part);
        ll_registration_second_part = findViewById(R.id.ll_registration_second_part);
        ll_button_holder = findViewById(R.id.ll_button_holder);

        edt_first_name = findViewById(R.id.edt_first_name);
        edt_last_name = findViewById(R.id.edt_last_name);
        edt_email_address = findViewById(R.id.edt_email_address);
        edt_confirm_email_address = findViewById(R.id.edt_confirm_email_address);

        edt_nickname = findViewById(R.id.edt_nickname);
        edt_date_of_birth = findViewById(R.id.edt_date_of_birth);
        edt_password = findViewById(R.id.edt_password);
        edt_confirm_password = findViewById(R.id.edt_confirm_password);

        chbx_terms_and_privacy = findViewById(R.id.chbx_terms_and_privacy);
        txt_legal_description = findViewById(R.id.txt_legal_description);

        btn_registration_back = findViewById(R.id.btn_registration_back);
        btn_registration_next = findViewById(R.id.btn_registration_next);

        txt_registration_messages = findViewById(R.id.txt_registration_messages);
        txt_login = findViewById(R.id.txt_login);
        txt_registration_team_name = findViewById(R.id.txt_registration_team_name);

        txt_registration_team_name.setText(""+CLUB_NAME);

//        edt_first_name.setText("CSUser001");
//        edt_last_name.setText("User");
//        edt_email_address.setText("CSUser001@gmail.com");
//        edt_confirm_email_address.setText("CSUser001@gmail.com");
//        edt_nickname.setText("CSUser001");
//        edt_password.setText("123456");
//        edt_confirm_password.setText("123456");


//        edt_date_of_birth.setText("Feb 07,2006");

        setLinkify();

    }

    private void setLinkify() {

        String termsAndConditions = "terms";
        String privacyPolicy = "privacy";
        String message = "I agree to the terms and privacy.";


      /*  Linkify.TransformFilter transformFilter = new Linkify.TransformFilter() {
            public final String transformUrl(final Matcher match, String url) {
                return "";
            } };

        txt_legal_description.setText(
                String.format(
                        message,
                        termsAndConditions,
                        privacyPolicy)
        );
        txt_legal_description.setMovementMethod(LinkMovementMethod.getInstance());

        Pattern termsAndConditionsMatcher = Pattern.compile(termsAndConditions);
        Linkify.addLinks(txt_legal_description, termsAndConditionsMatcher, "terms:",null,transformFilter);

        Pattern privacyPolicyMatcher = Pattern.compile(privacyPolicy);
        Linkify.addLinks(txt_legal_description, privacyPolicyMatcher, "privacy:",null,transformFilter);*/



        Spannable span = Spannable.Factory.getInstance().newSpannable("I agree to the " + termsAndConditions + " and " + privacyPolicy+".");

        ClickableSpan ClickTermsOfUses = new ClickableSpan() {
            @Override
            public void onClick(View v) {
//                Uri uriUrl = Uri.parse(BaseActivity.mLink_Terms_of_Use);
//                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
//                startActivity(launchBrowser);

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(BaseActivity.mLink_Terms_of_Use));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            } };

        ClickableSpan ClickPrivacyPolicy = new ClickableSpan() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(BaseActivity.mLink_Privacy_policy));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } };


        span.setSpan(ClickTermsOfUses, 15, 15 + termsAndConditions.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        span.setSpan(ClickPrivacyPolicy, 24, span.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        txt_legal_description.setText(span);

        txt_legal_description.setMovementMethod(LinkMovementMethod.getInstance());

    }


    private void SetListners() {

        txt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(mContext, LoginActivity.class);
                startActivity(i);
                finish();


            }
        });

        edt_date_of_birth.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        mContext, DatePickerListener, mCalender
                        .get(Calendar.YEAR), mCalender.get(Calendar.MONTH), mCalender.get(Calendar.DAY_OF_MONTH));

                datePickerDialog.getDatePicker().setMaxDate(mCalender.getTimeInMillis());
                datePickerDialog.show();

//                new DatePickerDialog(mContext, DatePickerListener, mCalender
//                        .get(Calendar.YEAR), mCalender.get(Calendar.MONTH),
//                        mCalender.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        btn_registration_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (registration_level == 2) {

                    registration_level = 1;
                    ll_registration_first_part.setVisibility(View.VISIBLE);
                    ll_registration_second_part.setVisibility(View.GONE);

                    btn_registration_next.setText("Next");

                } else if (registration_level == 1) {

                    finish();
                }
            }
        });

        btn_registration_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                hideKeyboard(RegistrationActivity.this);

                mPassword = edt_password.getText().toString();
                mConfirm_password = edt_confirm_password.getText().toString();
                mEmail = edt_email_address.getText().toString();
                mConfirm_email = edt_confirm_email_address.getText().toString();
                mFirstname = edt_first_name.getText().toString();
                mLastname = edt_last_name.getText().toString();

                if (registration_level == 1) {

                    if (edt_first_name.getText().toString().length() == 0) {
//                    edt_club_code.setError("Enter your club code to start your registration.");
                        Toast.makeText(mContext, "Please ensure you have entered your first name.", Toast.LENGTH_SHORT).show();
                    } else if (edt_last_name.getText().toString().length() == 0) {

                        Toast.makeText(mContext, "Please ensure you have entered your last name.", Toast.LENGTH_SHORT).show();

                    } else if (edt_email_address.getText().toString().length() == 0) {

                        Toast.makeText(mContext, "Please enter your email address.", Toast.LENGTH_SHORT).show();

                    } else if (!isEditTextContainEmail(edt_email_address)) {

                        Toast.makeText(mContext, "Please ensure you have entered a valid email address.", Toast.LENGTH_SHORT).show();

                    } else if (edt_confirm_email_address.getText().toString().length() == 0) {

                        Toast.makeText(mContext, "Please confirm your email address.", Toast.LENGTH_SHORT).show();

                    } else if (!mConfirm_email.equalsIgnoreCase(mEmail)) {

                        Toast.makeText(mContext, "Please confirm your email address.", Toast.LENGTH_SHORT).show();

                    } else {
//                        Toast.makeText(mContext, "Everything is fine", Toast.LENGTH_SHORT).show();
                        registration_level = 2;
                        ll_registration_first_part.setVisibility(View.GONE);
                        ll_registration_second_part.setVisibility(View.VISIBLE);

                        btn_registration_next.setText("Register");

                        edt_nickname.setText(""+mFirstname+mLastname.charAt(0));
                    }

                } else if (registration_level == 2) {


//                    if (edt_nickname.getText().toString().length() == 0) {
////                    edt_club_code.setError("Enter your club code to start your registration.");
//                        Toast.makeText(mContext, "Please ensure you have entered nickname.", Toast.LENGTH_SHORT).show();
//                    } else

                    if (edt_date_of_birth.getText().toString().length() == 0) {

                        Toast.makeText(mContext, "Please enter your date of birth.", Toast.LENGTH_SHORT).show();

                    } else if (mAge < 18) {

                        Toast.makeText(mContext, "You must be 18 years of age or older to register for Club Supporter.", Toast.LENGTH_SHORT).show();

                    } else if (edt_password.getText().toString().length() == 0) {

                        Toast.makeText(mContext, "Please enter a password.", Toast.LENGTH_SHORT).show();

                    } else if (edt_password.getText().toString().length() != 0 && edt_password.getText().toString().length() < 8) {

                        Toast.makeText(mContext, "Please ensure you choose a password that is at least 8 characters long and contains at least 1 number, one uppercase and one lowercase letter.", Toast.LENGTH_SHORT).show();

                    } else if (!mPasswordValidator.validate(mPassword)) {

//                        Toast.makeText(mContext, "Not valid Password.", Toast.LENGTH_SHORT).show();

                        Toast.makeText(mContext, "Please ensure you choose a password that is at least 8 characters long and contains at least 1 number, one uppercase and one lowercase letter.", Toast.LENGTH_SHORT).show();

                    } else if (!mConfirm_password.equalsIgnoreCase(mPassword)) {

                        Toast.makeText(mContext, "Please confirm your password.", Toast.LENGTH_SHORT).show();

                    } else if (!chbx_terms_and_privacy.isChecked()) {

                        Toast.makeText(mContext, "You must agree to the terms and privacy before registration can be completed.", Toast.LENGTH_SHORT).show();

                    } else {

//                        Toast.makeText(mContext, "Everything is super fine", Toast.LENGTH_SHORT).show();
                        sendRegistrationRequest();
                    }

                }

            }
        });

    }

    public static boolean isEditTextContainEmail(EditText argEditText) {

        try {
            Pattern pattern = Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
            Matcher matcher = pattern.matcher(argEditText.getText().toString().trim());
            return matcher.matches();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    DatePickerDialog.OnDateSetListener DatePickerListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            mCalender.set(Calendar.YEAR, year);
            mCalender.set(Calendar.MONTH, monthOfYear);
            mCalender.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            mAge = getAge(year, monthOfYear, dayOfMonth);
            Log.e("Calculate Age", "Get Age : " + getAge(year, monthOfYear, dayOfMonth));
            updateLabel();
        }

    };

    private void updateLabel() {
        String myFormat = "MMM dd,yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());

        edt_date_of_birth.setText(sdf.format(mCalender.getTime()));
    }

    private void sendRegistrationRequest() {

        showWaitIndicator(true);

//        mFirstname = edt_first_name.getText().toString();
//        mLastname = edt_first_name.getText().toString();
//        mEmail = edt_first_name.getText().toString();
//        mConfirm_email = edt_first_name.getText().toString();
        mNickname = edt_nickname.getText().toString();
        mDateofBirth = edt_date_of_birth.getText().toString();
//        mPassword = edt_first_name.getText().toString();
//        mConfirm_password = edt_first_name.getText().toString();
        mAgree = chbx_terms_and_privacy.isChecked();

        if(club_team == null){

            club_team = "";
        }

        BaseActivity.FCMTOKEN = getFCMTOKEN();

        Log.e("REGISTRATION_INFO", "Club Code : " + CLUB_CODE);
        Log.e("REGISTRATION_INFO", "Club Team : " + club_team);
        Log.e("REGISTRATION_INFO", "Firstname : " + mFirstname);
        Log.e("REGISTRATION_INFO", "Lastname : " + mLastname);
        Log.e("REGISTRATION_INFO", "Email : " + mEmail);
        Log.e("REGISTRATION_INFO", "Confirm Email : " + mConfirm_email);
        Log.e("REGISTRATION_INFO", "Nickname : " + mNickname);
        Log.e("REGISTRATION_INFO", "DateofBirth : " + mDateofBirth);
        Log.e("REGISTRATION_INFO", "Password : " + mPassword);
        Log.e("REGISTRATION_INFO", "Confirm Password : " + mConfirm_password);
        Log.e("REGISTRATION_INFO", "Agree : " + mAgree);
        Log.e("REGISTRATION_INFO", "FcmToken : " + BaseActivity.FCMTOKEN);

       Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<Registration> call = service.sendRegistrationRequest(CLUB_CODE, club_team, mFirstname, mLastname, mEmail, mConfirm_email, mNickname, mDateofBirth, mPassword, mConfirm_password, mAgree, BaseActivity.FCMTOKEN);

        call.enqueue(new Callback<Registration>() {
            @Override
            public void onResponse(Call<Registration> call, Response<Registration> response) {

                showWaitIndicator(false);

                Log.e("REGISTRATION_RESPONSE", "response code 200 : " + response.body());

                try {
                    if (response.code() == 200) {


                        Log.e("REGISTRATION_RESPONSE", "response code 200 : " + response.body().getFirstName());
                        Toast.makeText(mContext, "User Registered Successfully", Toast.LENGTH_SHORT).show();

                        txt_registration_messages.setText("Thank you for registering");

                        ll_registration_first_part.setVisibility(View.GONE);
                        ll_registration_second_part.setVisibility(View.GONE);
                        ll_button_holder.setVisibility(View.GONE);
                        txt_registration_team_name.setVisibility(View.GONE);

//                        Intent i = new Intent(mContext, DashBoardActivity.class);
//                        startActivity(i);

                    } else if (response.code() == 400) {


                        Gson gson = new GsonBuilder().create();
                        ForgotPassword mError = new ForgotPassword();
                        try {
                            mError = gson.fromJson(response.errorBody().string(), ForgotPassword.class);

                            Log.e("REGISTRATION_RESPONSE", "response code 400 : " + mError.getMessage());
                            Toast.makeText(mContext, " " + mError.getMessage(), Toast.LENGTH_SHORT).show();

                        } catch (Exception e) {
                            // handle failure to read error
                            Log.e("REGISTRATION_RESPONSE", "response code 400 Exception : " + e);
                        }


//                        Log.e("REGISTRATION_RESPONSE", "response code 200 : " + response.body());
//                        Toast.makeText(mContext, "Something went Wrong", Toast.LENGTH_SHORT).show();

                    } else if (response.code() == 500) {


                        Gson gson = new GsonBuilder().create();
                        ForgotPassword mError = new ForgotPassword();
                        try {
                            mError = gson.fromJson(response.errorBody().string(), ForgotPassword.class);

                            Log.e("REGISTRATION_RESPONSE", "response code 500 : " + mError.getMessage());
                            Toast.makeText(mContext, " " + mError.getMessage(), Toast.LENGTH_SHORT).show();

                        } catch (Exception e) {
                            // handle failure to read error
                            Log.e("REGISTRATION_RESPONSE", "response code 500 Exception : " + e);
                        }


                    }


                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    e.printStackTrace();
                    showWaitIndicator(false);
                }
            }

            @Override
            public void onFailure(Call<Registration> call, Throwable t) {
                showWaitIndicator(false);
                Log.d("onFailure", t.toString());

            }
        });
    }

    @Override
    protected void onNewIntent(final Intent intent) {


//        if (intent.getScheme().equals("terms")) {
////
////            mForLegal = "terms";
////            Toast.makeText(mContext, "onNewIntent", Toast.LENGTH_SHORT).show();
////
////        }else if(intent.getScheme().equals("privacy")){
////
////            mForLegal = "privacy";
////            Toast.makeText(mContext, "onNewIntent", Toast.LENGTH_SHORT).show();
////
////
////        }

//        Intent i = new Intent(mContext, LeagalDescriptionActivity.class);
//        startActivity(i);


    }

}
