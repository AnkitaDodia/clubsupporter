package com.sp.clubsupporter.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sp.clubsupporter.R;
import com.sp.clubsupporter.common.BaseActivity;
import com.sp.clubsupporter.models.ForgotPassword;
import com.sp.clubsupporter.restinterface.RestInterface;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ForgottenPasswordActivity extends BaseActivity {

    Context mContext;
    TextView txt_login;
    Button btn_submit;
    EditText edt_forgotten_email;
    TextView forgot_password_lable;

    String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotten_password);

        getSupportActionBar().hide();

        mContext = this;

        initviews();
        setListners();

        edt_forgotten_email.setVisibility(View.VISIBLE);
        btn_submit.setVisibility(View.VISIBLE);
        forgot_password_lable.setText(R.string.txt_msg_forgotten_password);

//        forgot_password_lable.setText("Password successfully reset. Please check your emails.");

    }

    private void initviews(){

        txt_login = findViewById(R.id.txt_login);

        edt_forgotten_email = findViewById(R.id.edt_forgotten_email);

        btn_submit = findViewById(R.id.btn_submit);

        forgot_password_lable = findViewById(R.id.forgot_password_lable);

//        edt_forgotten_email.setText("deeds@mailinator.com");

    }

    private void setListners(){

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                hideKeyboard(ForgottenPasswordActivity.this);
//                edt_forgotten_email.setText(edt_forgotten_email.getText().toString().trim());

                Log.e("email","email : "+edt_forgotten_email.getText().toString().trim());

                if (edt_forgotten_email.getText().toString().trim().length() == 0) {

//                    edt_forgotten_email.setError("Please enter email");
                    Toast.makeText(mContext,"Please ensure you have entered email address.",Toast.LENGTH_LONG).show();

                } else if (!isEditTextContainEmail(edt_forgotten_email)) {

//                    edt_forgotten_email.setError("Please enter valid email");
                    Toast.makeText(mContext,"Please ensure you have entered a valid email address.",Toast.LENGTH_LONG).show();

                }else {

                    sendForgotPasswordRequest();
                }

            }
        });

        txt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(mContext, LoginActivity.class);
                startActivity(i);
                finish();

            }
        });

        edt_forgotten_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable mEditable) {

//                edt_forgotten_email.setText(mEditable.toString().trim());

                if(TextUtils.isEmpty(mEditable.toString().trim())){
                    //write your code here
                    btn_submit.setBackgroundResource(R.drawable.btn_disable_selector);

                }else {

                    btn_submit.setBackgroundResource(R.drawable.btn_enable_selector);

                }

            }
        });


    }

    public static boolean isEditTextContainEmail(EditText argEditText) {

        try {
            Pattern pattern = Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
            Matcher matcher = pattern.matcher(argEditText.getText().toString().trim());
            return matcher.matches();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    private void sendForgotPasswordRequest() {

        showWaitIndicator(true);

        email = edt_forgotten_email.getText().toString().trim();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<ForgotPassword> call = service.sendForgotPasswordRequest(email);

        call.enqueue(new Callback<ForgotPassword>() {
            @Override
            public void onResponse(Call<ForgotPassword> call, Response<ForgotPassword> response) {

                showWaitIndicator(false);

                Log.d("FORGOT_PASSWORD", "RESPONSE code : " + response.code());

                try {
                    if (response.code() == 200) {

                        if (response.body().getMessage() != null) {

                            Log.d("FORGOT_PASSWORD", "RESPONSE code 200 : " + response.body().getMessage());

//                            Toast.makeText(mContext, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();

//                            Intent i = new Intent(mContext, LoginActivity.class);
//                            startActivity(i);
//                            finish();

                            edt_forgotten_email.setVisibility(View.GONE);
                            btn_submit.setVisibility(View.GONE);
                            forgot_password_lable.setText("Password successfully reset. Please check your emails.");
                        }


                    } else if (response.code() == 400) {


                        Gson gson = new GsonBuilder().create();
                        ForgotPassword mError = new ForgotPassword();
                        try {
                            mError = gson.fromJson(response.errorBody().string(), ForgotPassword.class);

                            Log.e("FORGOT_PASSWORD", "response code 400 : " + mError.getMessage());
                            Toast.makeText(mContext, ""+mError.getMessage(), Toast.LENGTH_SHORT).show();


                        } catch (Exception e) {
                            // handle failure to read error
                            Log.e("FORGOT_PASSWORD", "response code 400 Exception : " + e);
                        }

                    }


                } catch (Exception e) {
                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ForgotPassword> call, Throwable t) {
                showWaitIndicator(false);
                Log.d("onFailure", t.toString());

            }
        });

    }
}
