package com.sp.clubsupporter.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sp.clubsupporter.R;
import com.sp.clubsupporter.common.BaseActivity;
import com.sp.clubsupporter.models.Login;
import com.sp.clubsupporter.models.LoginErrors;
import com.sp.clubsupporter.restinterface.RestInterface;
import com.sp.clubsupporter.utility.InternetStatus;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class LoginActivity extends BaseActivity {

    Context mContext;
    NestedScrollView nsv_login;
    EditText edt_login_email, edt_login_password;
    Button btn_login;
    TextView txt_register, txt_forgotten_password;

    private boolean isInternet, isEmailEmpty = true, isPasswordEmpty = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getSupportActionBar().hide();

        mContext = this;
        isInternet = new InternetStatus().isInternetOn(mContext);

        initviews();
        setListners();

    }

    private void initviews() {

        edt_login_email = findViewById(R.id.edt_login_email);
        edt_login_password = findViewById(R.id.edt_login_password);

        txt_register = findViewById(R.id.txt_register);
        txt_forgotten_password = findViewById(R.id.txt_forgotten_password);

        btn_login = findViewById(R.id.btn_login);

        nsv_login = findViewById(R.id.nsv_login);

//        nsv_login.requestFocus();

//        edt_login_email.setText("deeds@mailinator.com");
//        edt_login_password.setText("Club123X");

//        edt_login_email.setText("CSUser001@gmail.com");
//        edt_login_password.setText("CCuser001");

//        edt_login_email.setText("phil@vmns.co.uk");
//        edt_login_password.setText("Benjamin1504");

//        edt_login_email.setSelection(edt_login_email.getText().length());
//        edt_login_password.setSelection(edt_login_password.getText().length());

    }

    private void setListners() {

        txt_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(mContext, CodeValidationActivity.class);
                startActivity(i);

            }
        });

        txt_forgotten_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(mContext, ForgottenPasswordActivity.class);
                startActivity(i);


            }
        });

        edt_login_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable mEditable) {

                if(TextUtils.isEmpty(mEditable.toString().trim())){
                    //write your code here
                    isEmailEmpty = true;
                }else {
                    isEmailEmpty = false;
                }

                EnableButton(isEmailEmpty, isPasswordEmpty);
            }
        });

        edt_login_password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable mEditable) {

                if(TextUtils.isEmpty(mEditable.toString().trim())){
                    //write your code here
                    isPasswordEmpty = true;
                }else {
                    isPasswordEmpty = false;

                }

                EnableButton(isEmailEmpty, isPasswordEmpty);
            }
        });


        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                hideKeyboard(LoginActivity.this);

                if (edt_login_email.getText().toString().trim().length() == 0) {
//                    edt_login_email.setError("Please enter email");
                    Toast.makeText(mContext,"Please ensure you have entered email address.",Toast.LENGTH_LONG).show();
                } else if (!isEditTextContainEmail(edt_login_email)) {
//                    edt_login_email.setError("Please enter valid email");
                    Toast.makeText(mContext,"Please ensure you have entered a valid email address.",Toast.LENGTH_LONG).show();
                } else if (edt_login_password.getText().toString().trim().length() == 0) {
//                    edt_login_password.setError("Please enter valid password");
                    Toast.makeText(mContext,"Please ensure you have entered password.",Toast.LENGTH_LONG).show();
                } else {
//                    if(!edt_login_email.getText().toString().equalsIgnoreCase(getLoginId()) ||
//                            !edt_password.getText().toString().equalsIgnoreCase(getLoginPwd()) && getRemember())
//                    {
//                        saveLoginDetail(edt_email.getText().toString(),edt_password.getText().toString());
//                    }
                    if (isInternet) {
                        sendLoginRequest();
                    } else {
                        Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

    }

    public static boolean isEditTextContainEmail(EditText argEditText) {

        try {
            Pattern pattern = Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
            Matcher matcher = pattern.matcher(argEditText.getText().toString().trim());
            return matcher.matches();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void EnableButton(boolean mIsEmail, boolean mIsPassword){

        if(mIsEmail && mIsPassword){

            btn_login.setBackgroundResource(R.drawable.btn_disable_selector);

        }else {

            btn_login.setBackgroundResource(R.drawable.btn_enable_selector);
        }

    }

//    dh7mUxP5Za0:APA91bG-RCd-NLFTIxnGkU96DEEVkgSruCGYQ9_APlat7tanfysLPbcA59SPT3jarfN7O-fYqBqHh5my-_PZDthR_onTTq37bAUhJACkopgXktHzoQOcvAPVX3KpouWH-AdGtVCGFMYq

    private void sendLoginRequest() {

        showWaitIndicator(true);

        final String email = edt_login_email.getText().toString().trim();
        String password = edt_login_password.getText().toString().trim();
        BaseActivity.FCMTOKEN = getFCMTOKEN();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Log.e("LOGIN_INFO", "Password : " + password);
        Log.e("LOGIN_INFO", "Confirm Password : " + email);
        Log.e("LOGIN_INFO", "FcmToken : " + BaseActivity.FCMTOKEN);


        Call<Login> call = service.sendLoginRequest("password", email, password, BaseActivity.FCMTOKEN);

        call.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {

                showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Log.d("LOGIN_RESPONSE", "response code 200 : " + response.body().getAccessToken());
                        Log.d("LOGIN_RESPONSE", "response code 200 : " + response.body().getClubid());

//                        Toast.makeText(mContext, "while Login ClubId : "+response.body().getClubid(), Toast.LENGTH_SHORT).show();

                        setLoginToken(response.body().getAccessToken());
                        setEmailId(response.body().getEmailid());
                        setClubId(response.body().getClubid());
                        setUserId(response.body().getUserid());

                        mUserID = response.body().getUserid();
                        mAccessToken = response.body().getAccessToken();
                        mEmailID = response.body().getEmailid();
                        mClubID = response.body().getClubid();
//                        mLoginList.add(response.body());

                        BaseActivity.mSwitchClub = false;
                        setLogin(1);
                        Intent i = new Intent(mContext, DashBoardActivity.class);
                        startActivity(i);

                    } else if (response.code() == 400) {

                        setLogin(0);
                        Gson gson = new GsonBuilder().create();
                        LoginErrors mError = new LoginErrors();
                        try {
                            mError = gson.fromJson(response.errorBody().string(), LoginErrors.class);

                            Log.d("LOGIN_RESPONSE", "response code 400 : " + mError.getErrorDescription());
                            Toast.makeText(mContext, ""+mError.getErrorDescription(), Toast.LENGTH_SHORT).show();

                        } catch (Exception e) {
                            // handle failure to read error
                            Log.e("LOGIN_RESPONSE", "response code 400 Exception : " + e);
                        }

                    }


                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                showWaitIndicator(false);
                Log.d("onFailure", t.toString());

            }
        });

    }




}
