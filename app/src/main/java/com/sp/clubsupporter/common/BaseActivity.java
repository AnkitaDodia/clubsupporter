package com.sp.clubsupporter.common;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.sp.clubsupporter.R;
import com.sp.clubsupporter.models.Login;
import com.sp.clubsupporter.models.MyClub;
import com.sp.clubsupporter.models.MyClubDisplay;
import com.sp.clubsupporter.models.SupporterRole;
import com.sp.clubsupporter.models.WhiteLablling;

import java.util.ArrayList;
import java.util.Calendar;

public class BaseActivity extends AppCompatActivity {

    ProgressDialog mProgressDialog;
    public static String CLUB_CODE  = null , CLUB_NAME = null, club_team = null;
    public static int registration_level = 0;

    public static String mLink_Privacy_policy = "https://clubsupporter.co.uk/privacy-policy/";
    public static String mLink_Terms_of_Use = "https://clubsupporter.co.uk/terms-conditions/";


    public static String WEBSITE_URL = "https://cssupporter.azurewebsites.net?";
    public static String GAMES_DASHBOARD_URL = "https://cssupporter.azurewebsites.net/mydashboard?";
    public static String MY_GAMES_URL = "https://cssupporter.azurewebsites.net/mygames?";
    public static String ADD_NEW_CLUBS_URL = " https://cssupporter.azurewebsites.net/myclub?navLink=ac&";
    public static String MY_OTHER_CLUBS_URL = "https://cssupporter.azurewebsites.net/myclub?navLink=ac&";
    public static String MY_CLUBS_URL = "https://cssupporter.azurewebsites.net/myclub?navLink=mc";
    public static String MY_SETTINGS_URL = "https://cssupporter.azurewebsites.net/mysettings?";
    public static String MY_DETAILS_URL = "https://cssupporter.azurewebsites.net/mysettings?navLink=md&";
    public static String MY_TRANSACTIONS_URL = "https://cssupporter.azurewebsites.net/mysettings?navLink=mt&";
    public static String MY_PASSWORD_URL = "https://cssupporter.azurewebsites.net/mysettings?navLink=mp&";
    public static String MY_CASH_FLOW_URL = "https://cssupporter.azurewebsites.net/mysettings?navLink=cf&";
    public static String MY_WITHDRAW_URL = "https://cssupporter.azurewebsites.net/mysettings?navLink=wd&";
    public static String MY_TAKE_A_BREAK_URL = "https://cssupporter.azurewebsites.net/mysettings?navLink=tb&";
//    public static String MY_RESPONSIBLE_GAMBLING = "https://cssupporter.azurewebsites.net/responsiblegambling?";
    public static String MY_RESPONSIBLE_GAMBLING = "https://cssupporter.azurewebsites.net/playingResponsibly?";



    public static String FCMTOKEN = null;

    public static String mForLegal = null;

    public static String mCurrentUrl = null;

    public static String mUserID = null;
    public static String mAccessToken = null;
    public static String mEmailID = null;
    public static String mCurrencySymbol = null;
    public static Double mBalance = null;
    public static String mLastLoginDate = null;
    public static int mClubID = 0;

    public static boolean mSwitchClub = false;
    public static boolean mIsPasswordChange = false;

//    public static boolean mIsLogOut = false;

//    public static ArrayList<Login> mLoginList = new ArrayList<>();

    public static ArrayList<SupporterRole> mSupporterRoleList = new ArrayList<>();
    public static ArrayList<MyClubDisplay> mMyClubDisplayList = new ArrayList<>();


    public static String LINK_COLOR = "#FFFFFF";
    public static String NAVIGATION_COLOR = "#053B45";
    public static String TOP_BAR_COLOR = "#FFFFFF";
    public static String CONTAINER_COLOR = "#6AC1B2";


    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }


    public void showWaitIndicator(boolean state) {
//        showWaitIndicator(state, "");
        if (state) {

            showProgressDialog();

        } else {

            dismissProgressDialog();
        }

    }

    public void showWaitIndicator(boolean state, String message) {
        try {
            try {

                if (state) {

                    mProgressDialog = new ProgressDialog(this, R.style.TransparentProgressDialog);
                    mProgressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
//                    mProgressDialog.setProgressDrawable(getResources().getDrawable(R.drawable.custom_progress_bar));
                    mProgressDialog.setCancelable(false);
                    mProgressDialog.show();

                } else {
                    mProgressDialog.dismiss();
                }

            } catch (Exception e) {


            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initProgressDialog() {
        Log.d("ProgressBar", "initProgressDialog()");
        mProgressDialog = new ProgressDialog(this,  R.style.TransparentProgressDialog);
//        mProgressDialog.setMessage(LOADING);
        mProgressDialog.setCancelable(false);
//        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            initProgressDialog();
        }
        if (mProgressDialog != null && !mProgressDialog.isShowing()) {
//            mProgressDialog.setMessage("LOADING...");
            mProgressDialog.show();
        }
    }

    public void showProgressDialog(String message) {
        if (mProgressDialog == null) {
            initProgressDialog();
        }
        mProgressDialog.setMessage(message);
        mProgressDialog.show();
    }

    public void dismissProgressDialog() {
        Log.d("ProgressBar", "dismissProgressDialog()");
        try {
            if (mProgressDialog != null) {
                mProgressDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public Integer getAge(int year, int month, int day) {
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.set(year, month, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }

        Integer ageInt = new Integer(age);
//        String ageS = ageInt.toString();

        return ageInt;
    }


    // Code for get and set login
    public void setLogin(int i)
    {
        SharedPreferences sp = getSharedPreferences("LOGIN",MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putInt("Login",i);
        spe.apply();
    }

    public int getLogin()
    {
        SharedPreferences sp = getSharedPreferences("LOGIN",MODE_PRIVATE);
        int i = sp.getInt("Login",0);
        return i;
    }
    // Code for get and set login

    // Code for get and set login
    public void setLoginToken(String token)
    {
        SharedPreferences sp = getSharedPreferences("LOGIN_TOKEN",MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putString("LoginToken", token);
        spe.apply();
    }

    public String getLoginToken()
    {
        SharedPreferences sp = getSharedPreferences("LOGIN_TOKEN",MODE_PRIVATE);
        String token = sp.getString("LoginToken",null);
        return token;
    }

    public void setUserId(String userid)
    {
        SharedPreferences sp = getSharedPreferences("USER_ID",MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putString("userid", userid);
        spe.apply();
    }

    public String getUserId()
    {
        SharedPreferences sp = getSharedPreferences("USER_ID",MODE_PRIVATE);
        String userid = sp.getString("userid",null);
        return userid;
    }

    public void setEmailId(String emailid)
    {
        SharedPreferences sp = getSharedPreferences("EMAIL_ID",MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putString("emailid", emailid);
        spe.apply();
    }

    public String getEmailId()
    {
        SharedPreferences sp = getSharedPreferences("EMAIL_ID",MODE_PRIVATE);
        String emailid = sp.getString("emailid",null);
        return emailid;
    }

    public void setClubId(int clubid)
    {
        SharedPreferences sp = getSharedPreferences("CLUB_ID",MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putInt("clubid", clubid);
        spe.apply();
    }

    public int getClubId()
    {
        SharedPreferences sp = getSharedPreferences("CLUB_ID",MODE_PRIVATE);
        int clubid = sp.getInt("clubid",0);
        return clubid;
    }



    // Code for get and set login


    // Code for get and set FCMTOKEN




    public String getFCMTOKEN()
    {
        SharedPreferences sp = getSharedPreferences("FCM",MODE_PRIVATE);
        String fcm_token = sp.getString("FCM_ID",null);
        return fcm_token;
    }
    // Code for get and set FCMTOKEN


    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


}


