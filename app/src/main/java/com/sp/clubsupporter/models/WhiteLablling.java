package com.sp.clubsupporter.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WhiteLablling {

    @SerializedName("Crest")
    @Expose
    private String crest;
    @SerializedName("HeaderColour")
    @Expose
    private String headerColour;
    @SerializedName("NavigationColour")
    @Expose
    private String navigationColour;
    @SerializedName("LinkColour")
    @Expose
    private String linkColour;
    @SerializedName("DarkColor")
    @Expose
    private String darkColor;
    @SerializedName("DisableStyling")
    @Expose
    private Boolean disableStyling;
    @SerializedName("OverrideStyling")
    @Expose
    private Boolean overrideStyling;
    @SerializedName("OpenCode")
    @Expose
    private Boolean openCode;

    public String getCrest() {
        return crest;
    }

    public void setCrest(String crest) {
        this.crest = crest;
    }

    public String getHeaderColour() {
        return headerColour;
    }

    public void setHeaderColour(String headerColour) {
        this.headerColour = headerColour;
    }

    public String getNavigationColour() {
        return navigationColour;
    }

    public void setNavigationColour(String navigationColour) {
        this.navigationColour = navigationColour;
    }

    public String getLinkColour() {
        return linkColour;
    }

    public void setLinkColour(String linkColour) {
        this.linkColour = linkColour;
    }

    public String getDarkColor() {
        return darkColor;
    }

    public void setDarkColor(String darkColor) {
        this.darkColor = darkColor;
    }

    public Boolean getDisableStyling() {
        return disableStyling;
    }

    public void setDisableStyling(Boolean disableStyling) {
        this.disableStyling = disableStyling;
    }

    public Boolean getOverrideStyling() {
        return overrideStyling;
    }

    public void setOverrideStyling(Boolean overrideStyling) {
        this.overrideStyling = overrideStyling;
    }

    public Boolean getOpenCode() {
        return openCode;
    }

    public void setOpenCode(Boolean openCode) {
        this.openCode = openCode;
    }
}
