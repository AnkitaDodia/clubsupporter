package com.sp.clubsupporter.models;

public class MyClubDisplay {

    private String name;
    private Integer clubId;
    private boolean CurrentClub;
    private boolean SignedUpClub;
    private Integer LiveGames;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getClubId() {
        return clubId;
    }

    public void setClubId(Integer clubId) {
        this.clubId = clubId;
    }

    public boolean isCurrentClub() {
        return CurrentClub;
    }

    public void setCurrentClub(boolean currentClub) {
        CurrentClub = currentClub;
    }

    public boolean isSignedUpClub() {
        return SignedUpClub;
    }

    public void setSignedUpClub(boolean signedUpClub) {
        SignedUpClub = signedUpClub;
    }



    public Integer getLiveGames() {
        return LiveGames;
    }

    public void setLiveGames(Integer liveGames) {
        LiveGames = liveGames;
    }


}
