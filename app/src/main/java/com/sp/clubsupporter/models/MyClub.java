package com.sp.clubsupporter.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyClub {

    @SerializedName("Club")
    @Expose
    private Club club;
    @SerializedName("LiveGames")
    @Expose
    private Integer liveGames;
    @SerializedName("DateAdded")
    @Expose
    private String dateAdded;
    @SerializedName("CurrentClub")
    @Expose
    private Boolean currentClub;
    @SerializedName("SignedUpClub")
    @Expose
    private Boolean signedUpClub;

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public Integer getLiveGames() {
        return liveGames;
    }

    public void setLiveGames(Integer liveGames) {
        this.liveGames = liveGames;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public Boolean getCurrentClub() {
        return currentClub;
    }

    public void setCurrentClub(Boolean currentClub) {
        this.currentClub = currentClub;
    }

    public Boolean getSignedUpClub() {
        return signedUpClub;
    }

    public void setSignedUpClub(Boolean signedUpClub) {
        this.signedUpClub = signedUpClub;
    }
}
