package com.sp.clubsupporter.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Club {

    @SerializedName("ClubId")
    @Expose
    private Integer clubId;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("GroundName")
    @Expose
    private String groundName;
    @SerializedName("Postcode")
    @Expose
    private String postcode;
    @SerializedName("GoogleMapUrl")
    @Expose
    private String googleMapUrl;
    @SerializedName("Crest")
    @Expose
    private String crest;
    @SerializedName("HeaderColour")
    @Expose
    private String headerColour;
    @SerializedName("NavigationColour")
    @Expose
    private String navigationColour;
    @SerializedName("LinkColour")
    @Expose
    private String linkColour;
    @SerializedName("DisableStyling")
    @Expose
    private Boolean disableStyling;
    @SerializedName("IsDeleted")
    @Expose
    private Boolean isDeleted;
    @SerializedName("Removed")
    @Expose
    private Boolean removed;
    @SerializedName("OverrideStyling")
    @Expose
    private Boolean overrideStyling;
    @SerializedName("SingleHeaderColour")
    @Expose
    private Boolean singleHeaderColour;
    @SerializedName("ClubGuid")
    @Expose
    private String clubGuid;
    @SerializedName("ClubType")
    @Expose
    private ClubType clubType;
    @SerializedName("DateAdded")
    @Expose
    private String dateAdded;
    @SerializedName("ShortName")
    @Expose
    private String shortName;
    @SerializedName("Nickname")
    @Expose
    private String nickname;
    @SerializedName("Website")
    @Expose
    private String website;
    @SerializedName("EnableSMS")
    @Expose
    private Boolean enableSMS;
    @SerializedName("EnableEmails")
    @Expose
    private Boolean enableEmails;
    @SerializedName("EnablePushNotifications")
    @Expose
    private Boolean enablePushNotifications;
    @SerializedName("Currency")
    @Expose
    private Currency currency;
    @SerializedName("Facebook")
    @Expose
    private String facebook;
    @SerializedName("Twitter")
    @Expose
    private String twitter;
    @SerializedName("Youtube")
    @Expose
    private String youtube;
    @SerializedName("Chairman")
    @Expose
    private String chairman;
    @SerializedName("Commission")
    @Expose
    private Integer commission;
    @SerializedName("AffiliateCommission")
    @Expose
    private Integer affiliateCommission;
    @SerializedName("StatusType")
    @Expose
    private Integer statusType;
    @SerializedName("StatusDate")
    @Expose
    private String statusDate;

    public Integer getClubId() {
        return clubId;
    }

    public void setClubId(Integer clubId) {
        this.clubId = clubId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroundName() {
        return groundName;
    }

    public void setGroundName(String groundName) {
        this.groundName = groundName;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getGoogleMapUrl() {
        return googleMapUrl;
    }

    public void setGoogleMapUrl(String googleMapUrl) {
        this.googleMapUrl = googleMapUrl;
    }

    public String getCrest() {
        return crest;
    }

    public void setCrest(String crest) {
        this.crest = crest;
    }

    public String getHeaderColour() {
        return headerColour;
    }

    public void setHeaderColour(String headerColour) {
        this.headerColour = headerColour;
    }

    public String getNavigationColour() {
        return navigationColour;
    }

    public void setNavigationColour(String navigationColour) {
        this.navigationColour = navigationColour;
    }

    public String getLinkColour() {
        return linkColour;
    }

    public void setLinkColour(String linkColour) {
        this.linkColour = linkColour;
    }

    public Boolean getDisableStyling() {
        return disableStyling;
    }

    public void setDisableStyling(Boolean disableStyling) {
        this.disableStyling = disableStyling;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Boolean getRemoved() {
        return removed;
    }

    public void setRemoved(Boolean removed) {
        this.removed = removed;
    }

    public Boolean getOverrideStyling() {
        return overrideStyling;
    }

    public void setOverrideStyling(Boolean overrideStyling) {
        this.overrideStyling = overrideStyling;
    }

    public Boolean getSingleHeaderColour() {
        return singleHeaderColour;
    }

    public void setSingleHeaderColour(Boolean singleHeaderColour) {
        this.singleHeaderColour = singleHeaderColour;
    }

    public String getClubGuid() {
        return clubGuid;
    }

    public void setClubGuid(String clubGuid) {
        this.clubGuid = clubGuid;
    }

    public ClubType getClubType() {
        return clubType;
    }

    public void setClubType(ClubType clubType) {
        this.clubType = clubType;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Boolean getEnableSMS() {
        return enableSMS;
    }

    public void setEnableSMS(Boolean enableSMS) {
        this.enableSMS = enableSMS;
    }

    public Boolean getEnableEmails() {
        return enableEmails;
    }

    public void setEnableEmails(Boolean enableEmails) {
        this.enableEmails = enableEmails;
    }

    public Boolean getEnablePushNotifications() {
        return enablePushNotifications;
    }

    public void setEnablePushNotifications(Boolean enablePushNotifications) {
        this.enablePushNotifications = enablePushNotifications;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getYoutube() {
        return youtube;
    }

    public void setYoutube(String youtube) {
        this.youtube = youtube;
    }

    public String getChairman() {
        return chairman;
    }

    public void setChairman(String chairman) {
        this.chairman = chairman;
    }

    public Integer getCommission() {
        return commission;
    }

    public void setCommission(Integer commission) {
        this.commission = commission;
    }

    public Integer getAffiliateCommission() {
        return affiliateCommission;
    }

    public void setAffiliateCommission(Integer affiliateCommission) {
        this.affiliateCommission = affiliateCommission;
    }

    public Integer getStatusType() {
        return statusType;
    }

    public void setStatusType(Integer statusType) {
        this.statusType = statusType;
    }

    public String getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(String statusDate) {
        this.statusDate = statusDate;
    }

}
