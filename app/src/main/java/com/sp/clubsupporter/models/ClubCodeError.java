package com.sp.clubsupporter.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClubCodeError {

    @SerializedName("Message")
    @Expose
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
