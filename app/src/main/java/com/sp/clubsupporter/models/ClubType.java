package com.sp.clubsupporter.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClubType {

    @SerializedName("ClubTypeId")
    @Expose
    private Integer clubTypeId;
    @SerializedName("Name")
    @Expose
    private String name;

    public Integer getClubTypeId() {
        return clubTypeId;
    }

    public void setClubTypeId(Integer clubTypeId) {
        this.clubTypeId = clubTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
