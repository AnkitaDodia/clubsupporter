package com.sp.clubsupporter.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FoundClubCode {

    @SerializedName("Code")
    @Expose
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
