package com.sp.clubsupporter.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SupporterRole {

    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("ClubId")
    @Expose
    private Integer clubId;
    @SerializedName("SignedUpClubId")
    @Expose
    private Integer signedUpClubId;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("EmailAddress")
    @Expose
    private String emailAddress;
    @SerializedName("ClubName")
    @Expose
    private ClubName clubName;
    @SerializedName("WebVersion")
    @Expose
    private Integer webVersion;
    @SerializedName("Balance")
    @Expose
    private Double balance;
    @SerializedName("TempPassword")
    @Expose
    private Boolean tempPassword;
    @SerializedName("Role")
    @Expose
    private String role;
    @SerializedName("LastLoginDate")
    @Expose
    private String lastLoginDate;
    @SerializedName("MyClubs")
    @Expose
    private List<MyClub> myClubs = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getClubId() {
        return clubId;
    }

    public void setClubId(Integer clubId) {
        this.clubId = clubId;
    }

    public Integer getSignedUpClubId() {
        return signedUpClubId;
    }

    public void setSignedUpClubId(Integer signedUpClubId) {
        this.signedUpClubId = signedUpClubId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public ClubName getClubName() {
        return clubName;
    }

    public void setClubName(ClubName clubName) {
        this.clubName = clubName;
    }

    public Integer getWebVersion() {
        return webVersion;
    }

    public void setWebVersion(Integer webVersion) {
        this.webVersion = webVersion;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Boolean getTempPassword() {
        return tempPassword;
    }

    public void setTempPassword(Boolean tempPassword) {
        this.tempPassword = tempPassword;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(String lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public List<MyClub> getMyClubs() {
        return myClubs;
    }

    public void setMyClubs(List<MyClub> myClubs) {
        this.myClubs = myClubs;
    }
}
