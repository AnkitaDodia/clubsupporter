package com.sp.clubsupporter.restinterface;

import com.sp.clubsupporter.models.Balance;
import com.sp.clubsupporter.models.ClubCode;
import com.sp.clubsupporter.models.ForgotPassword;
import com.sp.clubsupporter.models.FoundClubCode;
import com.sp.clubsupporter.models.FoundClubName;
import com.sp.clubsupporter.models.Login;
import com.sp.clubsupporter.models.Registration;
import com.sp.clubsupporter.models.SupporterRole;
import com.sp.clubsupporter.models.UpdateBalance;
import com.sp.clubsupporter.models.WhiteLablling;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;


/**
 * Created by codfidea on 09/22/2016.
 */
public interface RestInterface
{
    public static String API_BASE_URL = "https://cssupporterapi.azurewebsites.net";

    String LOGIN = "/token";
    String CODE_VALIDATION = "/api/supporter/validateclubcode";
    String CLUB_NAME = "/api/supporter/getclubcode";
    String REGISTRATION = "/api/supporter/register";
    String FORGOT_PASSWORD = "/api/supporter/forgottenpassword";
    String FOUND_CLUB_NAME = "/api/supporter/findclubcode";
    String WHITE_LABLEILING = "/api/supporter/getwhitelabelling";
    String GET_SUPPORTER_ROLE = "/api/supporter/getsupporterrole";
    String UPDATE_BALANCE = "/api/supporter/updatebalance";
    String REFRESH_BALANCE = "/api/supporter/refreshbalance";


//    https://cssupporterapi.azurewebsites.net/api/supporter/getwhitelabelling?userName=csuser005@gmail.com
//    https://cssupporterapi.azurewebsites.net/api/supporter/getsupporterrole?userName=csuser005@gmail.com
//    https://cssupporterapi.azurewebsites.net/api/supporter/findclubcode?code=c
//    https://cssupporterapi.azurewebsites.net/api/supporter/forgottenpassword?email=deeds@mailinator.com
//    https://cssupporterapi.azurewebsites.net/api/supporter/validateclubcode?code=CLSFC
//    https://cssupporterapi.azurewebsites.net/api/supporter/validateclubcode?foundClub={CLSFC}
//    https://cssupporterapi.azurewebsites.net/api/supporter/getclubcode?foundClub={Chester le Street Town FC}
//    https://cssupporterapi.azurewebsites.net/api/supporter/register


    @FormUrlEncoded
    @POST(LOGIN)
    Call<Login> sendLoginRequest(@Field("grant_type") String grant_type,
                                 @Field("username") String username,
                                 @Field("password") String password,
                                 @Field("fcmtoken") String fcmtoken);


    @GET(CODE_VALIDATION)
    Call<ClubCode> sendCodeValidationRequest(@Query("code") String code);

    @GET(CLUB_NAME)
    Call<FoundClubCode> sendClubNameRequest(@Query("foundClub") String clubname);

    @FormUrlEncoded
    @POST(REGISTRATION)
    Call<Registration> sendRegistrationRequest(@Field("ClubCode") String ClubCode,
                                               @Field("Club Team") String ClubTeam,
                                               @Field("FirstName") String FirstName,
                                               @Field("LastName") String LastName,
                                               @Field("EmailAddress") String EmailAddress,
                                               @Field("ConfirmEmailAddress") String ConfirmEmailAddress,
                                               @Field("NickName") String NickName,
                                               @Field("DateOfBirth") String DateOfBirth,
                                               @Field("Password") String Password,
                                               @Field("ConfirmPassword") String ConfirmPassword ,
                                               @Field("Agree") boolean Agree,
                                               @Field("fcmtoken") String fcmtoken);

    @GET(FORGOT_PASSWORD)
    Call<ForgotPassword> sendForgotPasswordRequest(@Query("email") String email);

    @GET(WHITE_LABLEILING)
    Call<WhiteLablling> sendWhiteLablingRequest(@Query("userName") String userName, @Query("clubId") int clubId, @Header("Authorization") String authHeader);

    @GET(GET_SUPPORTER_ROLE)
    Call<SupporterRole> sendSupporterRoleRequest(@Query("userName") String userName, @Query("clubId") int clubId, @Header("Authorization") String authHeader);

    @GET(FOUND_CLUB_NAME)
    Call<ArrayList<FoundClubName>> sendFoundClubnameRequest(@Query("code") String code);

    @GET(UPDATE_BALANCE)
    Call<UpdateBalance> sendUpdateBalanceRequest(@Query("cv2") String cv2, @Query("uid") String uid, @Query("amount") String amount, @Header("Authorization") String authHeader);

    @GET(REFRESH_BALANCE)
    Call<Balance> sendRefreshBalanceRequest(@Query("id") String id);
}
